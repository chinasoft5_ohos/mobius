package com.ohos.mobius;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.app.Context;

import com.ohos.mobius.slice.AddEditTaskAbilitySlice;

/**
 * AddEditTaskAbility
 */
public class AddEditTaskAbility extends Ability {
    /**
     *REQUEST ADD TASK
     */
    public static final int REQUEST_ADD_TASK = 1;

    /**
     * RESULT ADD TASK
     */
    public static final int RESULT_ADD_TASK = 2;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(AddEditTaskAbilitySlice.class.getName());
    }

    @Override
    protected void onActive() {
        super.onActive();
        Intent intent = new Intent();
        setResult(RESULT_ADD_TASK, intent);
    }

    /**
     * getIntent
     *
     * @param context context
     * @param toClass toClass
     * @return Intent
     */
    public static Intent getIntent(Context context, Class toClass) {
        if (context == null) {
            throw new NullPointerException("you are context is null");
        }
        Intent intent = new Intent();
        intent.setOperation(new Intent.OperationBuilder()
            .withBundleName(context.getBundleName())
            .withAbilityName(toClass.getName())
            .build());
        return intent;
    }
}
