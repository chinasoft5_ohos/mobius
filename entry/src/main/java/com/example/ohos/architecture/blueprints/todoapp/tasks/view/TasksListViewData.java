/*
 * -\-\-
 * --
 * Copyright (c) 2017-2018 Spotify AB
 * --
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * -/-/-
 */

package com.example.ohos.architecture.blueprints.todoapp.tasks.view;

import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;

public final class TasksListViewData {

    public static Builder builder() {
        return new TasksListViewData.Builder();
    }

    private int filterLabel;
    private boolean loading;
    private ViewState viewState;

    private TasksListViewData(
        int filterLabel,
        boolean loading,
        ViewState viewState) {
        this.filterLabel = filterLabel;
        this.loading = loading;
        this.viewState = viewState;
    }

    /**
     * filterLabel
     * @return int
     */
    @StringRes
    public int filterLabel() {
        return filterLabel;
    }

    /**
     * loading
     * @return boolean
     */
    public boolean loading() {
        return loading;
    }

    /**
     * viewState
     * @return ViewState
     */
    public ViewState viewState() {
        return viewState;
    }

    @Override
    public String toString() {
        return "TasksListViewData{"
            + "filterLabel=" + filterLabel + ", "
            + "loading=" + loading + ", "
            + "viewState=" + viewState
            + "}";
    }

    @Override
    public boolean equals(Object o1) {
        if (o1 == this) {
            return true;
        }
        if (o1 instanceof TasksListViewData) {
            TasksListViewData that = (TasksListViewData) o1;
            return (this.filterLabel == that.filterLabel())
                && (this.loading == that.loading())
                && (this.viewState.equals(that.viewState()));
        }
        return false;
    }

    @Override
    public int hashCode() {
        int temp = 1;
        temp *= 1000003;
        temp ^= filterLabel;
        temp *= 1000003;
        temp ^= loading ? 1231 : 1237;
        temp *= 1000003;
        temp ^= viewState.hashCode();
        return temp;
    }

    static final class Builder {
        private Integer filterLabel;
        private Boolean loading;
        private ViewState viewState;

        Builder() {
        }

        /**
         * filterLabel
         * @param filterLabel filterLabel
         * @return TasksListViewData.Builder
         */
        public TasksListViewData.Builder filterLabel(int filterLabel) {
            this.filterLabel = filterLabel;
            return this;
        }

        /**
         * loading
         * @param loading loading
         * @return TasksListViewData.Builder
         */
        public TasksListViewData.Builder loading(boolean loading) {
            this.loading = loading;
            return this;
        }

        /**
         * viewState
         * @param viewState viewState
         * @return TasksListViewData.Builder
         */
        public TasksListViewData.Builder viewState(ViewState viewState) {
            if (viewState == null) {
                throw new NullPointerException("Null viewState");
            }
            this.viewState = viewState;
            return this;
        }

        /**
         * build
         * @return TasksListViewData
         */
        public TasksListViewData build() {
            String missing = "";
            if (this.filterLabel == null) {
                missing += " filterLabel";
            }
            if (this.loading == null) {
                missing += " loading";
            }
            if (this.viewState == null) {
                missing += " viewState";
            }
            if (!missing.isEmpty()) {
                throw new IllegalStateException("Missing required properties:" + missing);
            }
            return new TasksListViewData(
                this.filterLabel,
                this.loading,
                this.viewState);
        }
    }

    /**
     * EmptyTasksViewData
     */
    public static final class EmptyTasksViewData {
        private final int title;
        private final int icon;
        private final int addViewVisibility;

        private EmptyTasksViewData(
            int title,
            int icon,
            int addViewVisibility) {
            this.title = title;
            this.icon = icon;
            this.addViewVisibility = addViewVisibility;
        }

        /**
         * builder
         * @return Builder
         */
        public Builder builder() {
            return new Builder();
        }

        @StringRes
        public int title() {
            return title;
        }

        /**
         * icon
         * @return int
         */
        @DrawableRes
        public int icon() {
            return icon;
        }

        /**
         * addViewVisibility
         * @return int
         */
        public int addViewVisibility() {
            return addViewVisibility;
        }

        @Override
        public String toString() {
            return "EmptyTasksViewData{"
                + "title=" + title + ", "
                + "icon=" + icon + ", "
                + "addViewVisibility=" + addViewVisibility
                + "}";
        }

        @Override
        public boolean equals(Object o1) {
            if (o1 == this) {
                return true;
            }
            if (o1 instanceof TasksListViewData.EmptyTasksViewData) {
                TasksListViewData.EmptyTasksViewData that = (TasksListViewData.EmptyTasksViewData) o1;
                return (this.title == that.title())
                    && (this.icon == that.icon())
                    && (this.addViewVisibility == that.addViewVisibility());
            }
            return false;
        }

        @Override
        public int hashCode() {
            int temp = 1;
            temp *= 1000003;
            temp ^= title;
            temp *= 1000003;
            temp ^= icon;
            temp *= 1000003;
            temp ^= addViewVisibility;
            return temp;
        }

        /**
         * Builder
         */
        public static final class Builder {
            private Integer title;
            private Integer icon;
            private Integer addViewVisibility;

            public Builder() {
            }

            /**
             * title
             * @param title title
             * @return  EmptyTasksViewData.Builder
             */
            public TasksListViewData.EmptyTasksViewData.Builder title(int title) {
                this.title = title;
                return this;
            }

            /**
             * icon
             * @param icon icon
             * @return Builder
             */
            public TasksListViewData.EmptyTasksViewData.Builder icon(int icon) {
                this.icon = icon;
                return this;
            }

            /**
             * addViewVisibility
             * @param addViewVisibility addViewVisibility
             * @return EmptyTasksViewData
             */
            public TasksListViewData.EmptyTasksViewData.Builder addViewVisibility(int addViewVisibility) {
                this.addViewVisibility = addViewVisibility;
                return this;
            }

            /**
             * build
             * @return TasksListViewData.EmptyTasksViewData
             */
            public TasksListViewData.EmptyTasksViewData build() {
                String missing = "";
                if (this.title == null) {
                    missing += " title";
                }
                if (this.icon == null) {
                    missing += " icon";
                }
                if (this.addViewVisibility == null) {
                    missing += " addViewVisibility";
                }
                if (!missing.isEmpty()) {
                    throw new IllegalStateException("Missing required properties:" + missing);
                }
                return new TasksListViewData.EmptyTasksViewData(
                    this.title,
                    this.icon,
                    this.addViewVisibility);
            }
        }
    }

    /**
     * TaskViewData
     */
    public static final class TaskViewData {
        private String title;
        private boolean completed;
        private int backgroundDrawableId;
        private String id;

        TaskViewData(String title, boolean completed, int backgroundDrawableId, String id) {
            if (title == null) {
                throw new NullPointerException("Null title");
            }
            this.title = title;
            this.completed = completed;
            this.backgroundDrawableId = backgroundDrawableId;
            if (id == null) {
                throw new NullPointerException("Null id");
            }
            this.id = id;
        }

        /**
         * create TaskViewData
         * @param title title
         * @param completed completed
         * @param backgroundDrawableId backgroundDrawableId
         * @param id id
         * @return TaskViewData
         */
        public static TaskViewData create(
            String title, boolean completed, int backgroundDrawableId, String id) {
            return new TaskViewData(
                title, completed, backgroundDrawableId, id);
        }

        /**
         * title
         * @return String
         */
        public String title() {
            return title;
        }

        /**
         * completed
         *
         * @return boolean
         */
        public boolean completed() {
            return completed;
        }

        /**
         * backgroundDrawableId
         *
         * @return int
         */
        @DrawableRes
        public int backgroundDrawableId() {
            return backgroundDrawableId;
        }

        /**
         * id
         *
         * @return String
         */
        public String id() {
            return id;
        }

        @Override
        public String toString() {
            return "TaskViewData{"
                + "title=" + title + ", "
                + "completed=" + completed + ", "
                + "backgroundDrawableId=" + backgroundDrawableId + ", "
                + "id=" + id
                + "}";
        }

        @Override
        public boolean equals(Object o1) {
            if (o1 == this) {
                return true;
            }
            if (o1 instanceof TasksListViewData.TaskViewData) {
                TasksListViewData.TaskViewData that = (TasksListViewData.TaskViewData) o1;
                return (this.title.equals(that.title()))
                    && (this.completed == that.completed())
                    && (this.backgroundDrawableId == that.backgroundDrawableId())
                    && (this.id.equals(that.id()));
            }
            return false;
        }

        @Override
        public int hashCode() {
            int temp = 1;
            temp *= 1000003;
            temp ^= title.hashCode();
            temp *= 1000003;
            temp ^= completed ? 1231 : 1237;
            temp *= 1000003;
            temp ^= backgroundDrawableId;
            temp *= 1000003;
            temp ^= id.hashCode();
            return temp;
        }

    }
}
