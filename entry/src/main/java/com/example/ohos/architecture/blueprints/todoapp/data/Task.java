/*
 * -\-\-
 * --
 * Copyright (c) 2017-2018 Spotify AB
 * --
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * -/-/-
 */

package com.example.ohos.architecture.blueprints.todoapp.data;

import java.io.Serializable;

/**
 * Immutable model class for a Task.
 */
public class Task implements Serializable {
    private String id = "";
    private TaskDetails details = null;

    /**
     * Task
     *
     * @param id id
     * @param details details
     */
    public Task(String id, TaskDetails details) {
        if (id == null) {
            throw new NullPointerException("Null id");
        }
        this.id = id;
        if (details == null) {
            throw new NullPointerException("Null details");
        }
        this.details = details;
    }

    public Task() {
    }

    /**
     * id
     *
     * @return id
     */
    public String id() {
        return id;
    }

    /**
     * details
     *
     * @return TaskDetails
     */
    public TaskDetails details() {
        return details;
    }

    @Override
    public String toString() {
        return "Task{"
            + "id=" + id + ", "
            + "details=" + details
            + "}";
    }

    /**
     * create
     *
     * @param id id
     * @param details details
     * @return Task
     */
    public static Task create(String id, TaskDetails details) {
        return new Task(id, details);
    }

    /**
     * complete
     *
     * @return Task
     */
    public Task complete() {
        return withDetails(details().withCompleted(true));
    }

    /**
     * activate
     *
     * @return Task
     */
    public Task activate() {
        return withDetails(details().withCompleted(false));
    }

    /**
     * withDetails
     *
     * @param details details
     * @return Task
     */
    public Task withDetails(TaskDetails details) {
        return create(id(), details);
    }


}
