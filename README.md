# mobius
#### 项目介绍
- 项目名称：mobius
- 所属系列：openharmony的第三方组件适配移植
- 功能：是一个用于管理状态演变的函数式反应框架
- 项目移植状态：主功能完成
- 调用差异：有(缺少LiveData实现)
- 开发版本：sdk6，DevEco Studio2.2 beta1
- 基线版本：Release 2.3.1

#### 效果演示
![avatar](screenshot/mobius.gif)

#### 安装教程
1.在项目根目录下的build.gradle文件中,
 ```
repositories {
    maven {
        url 'https://s01.oss.sonatype.org/content/repositories/releases/'
    }
}
 ```

2.在entry模块的build.gradle文件中，
 ```
dependencies {
    implementation 'com.spotify.mobius:mobius-core:1.5.3'
    implementation 'com.spotify.mobius:mobius-rx2:1.5.3'      // only for RxJava 2 support
    implementation 'com.spotify.mobius:mobius-extras:1.5.3'   // utilities for common patterns
    implementation 'com.gitee.chinasoft_ohos:mobius-ohos:1.0.0'
    
    implementation "io.reactivex.rxjava2:rxjava:2.1.9"
    implementation "com.squareup.sqlbrite2:sqlbrite:2.0.0"
    implementation 'com.spotify.dataenum:dataenum:1.3.1'
    annotationProcessor 'com.spotify.dataenum:dataenum-processor:1.3.1'
    
    implementation 'javax.annotation:jsr250-api:1.0'
}
 ```

在sdk6，DevEco Studio2.2 beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明
```java
  //生成一个模型并拓展更新函数
  enum CounterEvent {
    INCREMENT,
    DECREMENT,
  }
  class CounterLogic implements Update<Integer, CounterEvent, CounterEffect> {
    public Next<Integer, CounterEffect> update(Integer model, CounterEvent event) {
      switch (event) {
        case INCREMENT:
          return next(model + 1);
        case DECREMENT:
          if (model == 0) {
            Set<CounterEffect> soundEffect = effects(CounterEffect.PLAY_SOUND);
            return dispatch(soundEffect);
          } else {
            return next(model - 1);
          }
      }
      throw new IllegalStateException("Unhandled event: " + event);
    }
  }
  //实现Connectable接口来创建EffectHandler
  class CounterEffectHandler implements Connectable<CounterEffect, CounterEvent> {
    public Connection<CounterEffect> connect(Consumer<CounterEvent> output) {
      return new Connection<CounterEffect>() {
        @Override
        public void accept(CounterEffect effect) {
          if (effect == CounterEffect.PLAY_SOUND) {
            Toolkit.getDefaultToolkit().beep();
          }
        }
  
        @Override
        public void dispose() {}
      };
    }
  }
  //执行参考示例程序
  public static void main(String[] args) {
    // Let's make a Mobius Loop
    MobiusLoop<Integer, CounterEvent, CounterEffect> loop = Mobius
        .loop(new CounterLogic(), new CounterEffectHandler())
        .startFrom(0);
  
    // And start using our loop
    loop.dispatchEvent(CounterEvent.INCREMENT); // Model is now 1
    loop.dispatchEvent(CounterEvent.DECREMENT); // Model is now 0
    loop.dispatchEvent(CounterEvent.DECREMENT); // Sound effect plays! Model is still 0
  }
```


#### 测试信息
CodeCheck代码测试无异常  

CloudTest代码测试无异常  

病毒安全检测通过  

当前版本demo功能与原组件基本无差异

#### 版本迭代
- 1.0.0

#### 版权和许可信息
[Apache License, Version 2.0](LICENSE)

