/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.ohos.architecture.blueprints.todoapp.data;

/**
 * @since 2021-07-14
 */
public class TaskDisc {
    String id;
    private String title;
    private String description;
    private boolean completed;

    /**
     * TaskDisc
     *
     * @param id id
     * @param title title
     * @param description description
     * @param completed completed
     */
    public TaskDisc(String id, String title, String description, boolean completed) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.completed = completed;
    }

    /**
     * getId
     *
     * @return String
     */
    public String getId() {
        return id;
    }

    /**
     * setId
     *
     * @param id id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * getTitle
     *
     * @return String
     */
    public String getTitle() {
        return title;
    }

    /**
     * setTitle
     *
     * @param title title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * getDescription
     *
     * @return String
     */
    public String getDescription() {
        return description;
    }

    /**
     * setDescription
     *
     * @param description description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * isCompleted
     *
     * @return boolean
     */
    public boolean isCompleted() {
        return completed;
    }

    /**
     * setCompleted
     *
     * @param completed completed
     */
    public void setCompleted(boolean completed) {
        this.completed = completed;
    }
}
