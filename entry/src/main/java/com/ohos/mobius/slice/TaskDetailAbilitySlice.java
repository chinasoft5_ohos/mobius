package com.ohos.mobius.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;

import com.example.ohos.architecture.blueprints.todoapp.data.Task;
import com.example.ohos.architecture.blueprints.todoapp.taskdetail.TaskDetailInjector;
import com.example.ohos.architecture.blueprints.todoapp.taskdetail.domain.TaskDetailEvent;
import com.example.ohos.architecture.blueprints.todoapp.taskdetail.effecthandlers.TaskDetailEffectHandlers;
import com.example.ohos.architecture.blueprints.todoapp.taskdetail.view.TaskDetailViewDataMapper;
import com.example.ohos.architecture.blueprints.todoapp.taskdetail.view.TaskDetailViews;
import com.ohos.mobius.AddEditTaskAbility;
import com.ohos.mobius.ResourceTable;
import com.ohos.mobius.TaskDetailAbility;
import com.spotify.mobius.MobiusLoop;

import android.support.annotation.NonNull;
import io.reactivex.subjects.PublishSubject;

import static com.example.ohos.architecture.blueprints.todoapp.taskdetail.domain.TaskDetailEvent.deleteTaskRequested;
import static com.spotify.mobius.extras.Connectables.contramap;

/**
 * TaskDetailAbilitySlice
 */
public class TaskDetailAbilitySlice extends AbilitySlice {
    private TaskDetailViews mTaskDetailsViews;
    private Task task;

    private PublishSubject<TaskDetailEvent> mMenuEvents = PublishSubject.create();
    private MobiusLoop.Controller<Task, TaskDetailEvent> mController;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        getWindow().setStatusBarColor(getColor(ResourceTable.Color_colorTitle));
        super.setUIContent(ResourceTable.Layout_ability_task_detail);

        if (intent != null) {
            task = intent.getSerializableParam("taskob");
        }

        mTaskDetailsViews = new TaskDetailViews(getContext(), this, mMenuEvents);
        mController =
            TaskDetailInjector.createController(
                TaskDetailEffectHandlers.createEffectHandlers(
                    mTaskDetailsViews, getContext(), this::dismiss, this::openTaskEditor),
                task);
        mController.connect(contramap(TaskDetailViewDataMapper::taskToTaskViewData, mTaskDetailsViews));

        findComponentById(ResourceTable.Id_bt_delete).setClickedListener((component) -> {
            mMenuEvents.onNext(deleteTaskRequested());
        });

        findComponentById(ResourceTable.Id_iv_left).setClickedListener((component) -> {
            terminateAbility();
        });
    }

    @Override
    public void onActive() {
        super.onActive();
        mController.start();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
        mController.stop();
    }

    private void dismiss() {
        terminateAbility();
    }

    private void openTaskEditor(@NonNull Task task) {
        startAbilityForResult(TaskDetailAbility.getIntent(getContext(), AddEditTaskAbility.class, task), AddEditTaskAbility.RESULT_ADD_TASK);
    }

    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
        if (resultCode == AddEditTaskAbility.RESULT_ADD_TASK) {
            terminateAbility();
        }
    }
}
