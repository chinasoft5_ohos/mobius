package com.example.ohos.architecture.blueprints.todoapp.taskdetail.domain;

import com.example.ohos.architecture.blueprints.todoapp.data.Task;
import com.spotify.dataenum.function.Consumer;
import com.spotify.dataenum.function.Function;

import javax.annotation.Generated;
import javax.annotation.Nonnull;

import static com.spotify.dataenum.DataenumUtils.checkNotNull;

/**
 * TaskDetailEffect
 */
@Generated("com.spotify.dataenum.processor.DataEnumProcessor")
public abstract class TaskDetailEffect {
    TaskDetailEffect() {
    }

    /**
     * deleteTask
     *
     * @param task task
     * @return TaskDetailEffect
     */
    public static TaskDetailEffect deleteTask(@Nonnull Task task) {
        return new DeleteTask(task);
    }

    /**
     * saveTask
     *
     * @param task task
     * @return TaskDetailEffect
     */
    public static TaskDetailEffect saveTask(@Nonnull Task task) {
        return new SaveTask(task);
    }

    /**
     * notifyTaskMarkedComplete
     *
     * @return TaskDetailEffect
     */
    public static TaskDetailEffect notifyTaskMarkedComplete() {
        return new NotifyTaskMarkedComplete();
    }

    /**
     * notifyTaskMarkedActive
     *
     * @return TaskDetailEffect
     */
    public static TaskDetailEffect notifyTaskMarkedActive() {
        return new NotifyTaskMarkedActive();
    }

    /**
     * notifyTaskSaveFailed
     *
     * @return TaskDetailEffect
     */
    public static TaskDetailEffect notifyTaskSaveFailed() {
        return new NotifyTaskSaveFailed();
    }

    /**
     * notifyTaskDeletionFailed
     *
     * @return TaskDetailEffect
     */
    public static TaskDetailEffect notifyTaskDeletionFailed() {
        return new NotifyTaskDeletionFailed();
    }

    /**
     * openTaskEditor
     *
     * @param task task
     * @return TaskDetailEffect
     */
    public static TaskDetailEffect openTaskEditor(@Nonnull Task task) {
        return new OpenTaskEditor(task);
    }

    /**
     * exit
     *
     * @return TaskDetailEffect
     */
    public static TaskDetailEffect exit() {
        return new Exit();
    }

    /**
     * isDeleteTask
     *
     * @return boolean
     */
    public final boolean isDeleteTask() {
        return (this instanceof DeleteTask);
    }

    /**
     * isSaveTask
     *
     * @return boolean
     */
    public final boolean isSaveTask() {
        return (this instanceof SaveTask);
    }

    /**
     * isNotifyTaskMarkedComplete
     *
     * @return boolean
     */
    public final boolean isNotifyTaskMarkedComplete() {
        return (this instanceof NotifyTaskMarkedComplete);
    }

    /**
     * isNotifyTaskMarkedActive
     *
     * @return boolean
     */
    public final boolean isNotifyTaskMarkedActive() {
        return (this instanceof NotifyTaskMarkedActive);
    }

    /**
     * isNotifyTaskSaveFailed
     *
     * @return boolean
     */
    public final boolean isNotifyTaskSaveFailed() {
        return (this instanceof NotifyTaskSaveFailed);
    }

    /**
     * isNotifyTaskDeletionFailed
     *
     * @return boolean
     */
    public final boolean isNotifyTaskDeletionFailed() {
        return (this instanceof NotifyTaskDeletionFailed);
    }

    /**
     * isOpenTaskEditor
     *
     * @return boolean
     */
    public final boolean isOpenTaskEditor() {
        return (this instanceof OpenTaskEditor);
    }

    /**
     * isExit
     *
     * @return boolean
     */
    public final boolean isExit() {
        return (this instanceof Exit);
    }

    /**
     * asDeleteTask
     *
     * @return DeleteTask
     */
    public final DeleteTask asDeleteTask() {
        return (DeleteTask) this;
    }

    /**
     * asSaveTask
     *
     * @return SaveTask
     */
    public final SaveTask asSaveTask() {
        return (SaveTask) this;
    }

    /**
     * asNotifyTaskMarkedComplete
     *
     * @return NotifyTaskMarkedComplete
     */
    public final NotifyTaskMarkedComplete asNotifyTaskMarkedComplete() {
        return (NotifyTaskMarkedComplete) this;
    }

    /**
     * asNotifyTaskMarkedActive
     *
     * @return NotifyTaskMarkedActive
     */
    public final NotifyTaskMarkedActive asNotifyTaskMarkedActive() {
        return (NotifyTaskMarkedActive) this;
    }

    /**
     * asNotifyTaskSaveFailed
     * @return NotifyTaskSaveFailed
     */
    public final NotifyTaskSaveFailed asNotifyTaskSaveFailed() {
        return (NotifyTaskSaveFailed) this;
    }

    /**
     * asNotifyTaskDeletionFailed
     *
     * @return NotifyTaskDeletionFailed
     */
    public final NotifyTaskDeletionFailed asNotifyTaskDeletionFailed() {
        return (NotifyTaskDeletionFailed) this;
    }

    /**
     * asOpenTaskEditor
     *
     * @return OpenTaskEditor
     */
    public final OpenTaskEditor asOpenTaskEditor() {
        return (OpenTaskEditor) this;
    }

    /**
     * asExit
     *
     * @return Exit
     */
    public final Exit asExit() {
        return (Exit) this;
    }

    /**
     * match
     *
     * @param deleteTask deleteTask
     * @param saveTask saveTask
     * @param notifyTaskMarkedComplete notifyTaskMarkedComplete
     * @param notifyTaskMarkedActive notifyTaskMarkedActive
     * @param notifyTaskSaveFailed notifyTaskSaveFailed
     * @param notifyTaskDeletionFailed notifyTaskDeletionFailed
     * @param openTaskEditor openTaskEditor
     * @param exit exit
     */
    public abstract void match(@Nonnull Consumer<DeleteTask> deleteTask,
        @Nonnull Consumer<SaveTask> saveTask,@Nonnull Consumer<NotifyTaskMarkedComplete> notifyTaskMarkedComplete,
        @Nonnull Consumer<NotifyTaskMarkedActive> notifyTaskMarkedActive,
        @Nonnull Consumer<NotifyTaskSaveFailed> notifyTaskSaveFailed,
        @Nonnull Consumer<NotifyTaskDeletionFailed> notifyTaskDeletionFailed,
        @Nonnull Consumer<OpenTaskEditor> openTaskEditor, @Nonnull Consumer<Exit> exit);

    /**
     * map
     *
     * @param deleteTask deleteTask
     * @param saveTask saveTask
     * @param notifyTaskMarkedComplete notifyTaskMarkedComplete
     * @param notifyTaskMarkedActive notifyTaskMarkedActive
     * @param notifyTaskSaveFailed notifyTaskSaveFailed
     * @param notifyTaskDeletionFailed notifyTaskDeletionFailed
     * @param openTaskEditor openTaskEditor
     * @param exit exit
     * @param <R_> R_
     * @return R_
     */
    public abstract <R_> R_ map(@Nonnull Function<DeleteTask, R_> deleteTask,
        @Nonnull Function<SaveTask, R_> saveTask,
        @Nonnull Function<NotifyTaskMarkedComplete, R_> notifyTaskMarkedComplete,
        @Nonnull Function<NotifyTaskMarkedActive, R_> notifyTaskMarkedActive,
        @Nonnull Function<NotifyTaskSaveFailed, R_> notifyTaskSaveFailed,
        @Nonnull Function<NotifyTaskDeletionFailed, R_> notifyTaskDeletionFailed,
        @Nonnull Function<OpenTaskEditor, R_> openTaskEditor, @Nonnull Function<Exit, R_> exit);

    /**
     * DeleteTask
     */
    public static final class DeleteTask extends TaskDetailEffect {
        private final Task task;

        DeleteTask(Task task) {
            this.task = checkNotNull(task);
        }

        /**
         * task
         * @return Task
         */
        @Nonnull
        public final Task task() {
            return task;
        }

        @Override
        public boolean equals(Object other) {
            if (other == this) {
                return true;
            }
            if (!(other instanceof DeleteTask)){
                return false;
            }
            DeleteTask o1 = (DeleteTask) other;
            return o1.task.equals(this.task);
        }

        @Override
        public int hashCode() {
            int result = 0;
            return result * 31 + task.hashCode();
        }

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append("DeleteTask{task=").append(task);
            return builder.append('}').toString();
        }

        @Override
        public final void match(@Nonnull Consumer<DeleteTask> deleteTask,
            @Nonnull Consumer<SaveTask> saveTask,
            @Nonnull Consumer<NotifyTaskMarkedComplete> notifyTaskMarkedComplete,
            @Nonnull Consumer<NotifyTaskMarkedActive> notifyTaskMarkedActive,
            @Nonnull Consumer<NotifyTaskSaveFailed> notifyTaskSaveFailed,
            @Nonnull Consumer<NotifyTaskDeletionFailed> notifyTaskDeletionFailed,
            @Nonnull Consumer<OpenTaskEditor> openTaskEditor, @Nonnull Consumer<Exit> exit) {
            deleteTask.accept(this);
        }

        @Override
        public final <R_> R_ map(@Nonnull Function<DeleteTask, R_> deleteTask,
            @Nonnull Function<SaveTask, R_> saveTask,
            @Nonnull Function<NotifyTaskMarkedComplete, R_> notifyTaskMarkedComplete,
            @Nonnull Function<NotifyTaskMarkedActive, R_> notifyTaskMarkedActive,
            @Nonnull Function<NotifyTaskSaveFailed, R_> notifyTaskSaveFailed,
            @Nonnull Function<NotifyTaskDeletionFailed, R_> notifyTaskDeletionFailed,
            @Nonnull Function<OpenTaskEditor, R_> openTaskEditor, @Nonnull Function<Exit, R_> exit) {
            return deleteTask.apply(this);
        }
    }

    /**
     * SaveTask
     */
    public static final class SaveTask extends TaskDetailEffect {
        private final Task task;

        SaveTask(Task task) {
            this.task = checkNotNull(task);
        }

        /**
         * Task
         *
         * @return Task
         */
        @Nonnull
        public final Task task() {
            return task;
        }

        @Override
        public boolean equals(Object other) {
            if (other == this){
                return true;
            }
            if (!(other instanceof SaveTask)) {
                return false;
            }
            SaveTask o1 = (SaveTask) other;
            return o1.task.equals(this.task);
        }

        @Override
        public int hashCode() {
            int result = 0;
            return result * 31 + task.hashCode();
        }

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append("SaveTask{task=").append(task);
            return builder.append('}').toString();
        }

        @Override
        public final void match(@Nonnull Consumer<DeleteTask> deleteTask,
            @Nonnull Consumer<SaveTask> saveTask,
            @Nonnull Consumer<NotifyTaskMarkedComplete> notifyTaskMarkedComplete,
            @Nonnull Consumer<NotifyTaskMarkedActive> notifyTaskMarkedActive,
            @Nonnull Consumer<NotifyTaskSaveFailed> notifyTaskSaveFailed,
            @Nonnull Consumer<NotifyTaskDeletionFailed> notifyTaskDeletionFailed,
            @Nonnull Consumer<OpenTaskEditor> openTaskEditor, @Nonnull Consumer<Exit> exit) {
            saveTask.accept(this);
        }

        @Override
        public final <R_> R_ map(@Nonnull Function<DeleteTask, R_> deleteTask,
            @Nonnull Function<SaveTask, R_> saveTask,
            @Nonnull Function<NotifyTaskMarkedComplete, R_> notifyTaskMarkedComplete,
            @Nonnull Function<NotifyTaskMarkedActive, R_> notifyTaskMarkedActive,
            @Nonnull Function<NotifyTaskSaveFailed, R_> notifyTaskSaveFailed,
            @Nonnull Function<NotifyTaskDeletionFailed, R_> notifyTaskDeletionFailed,
            @Nonnull Function<OpenTaskEditor, R_> openTaskEditor, @Nonnull Function<Exit, R_> exit) {
            return saveTask.apply(this);
        }
    }

    /**
     * NotifyTaskMarkedComplete
     */
    public static final class NotifyTaskMarkedComplete extends TaskDetailEffect {
        NotifyTaskMarkedComplete() {
        }

        @Override
        public boolean equals(Object other) {
            return other instanceof NotifyTaskMarkedComplete;
        }

        @Override
        public int hashCode() {
            return 0;
        }

        @Override
        public String toString() {
            return "NotifyTaskMarkedComplete{}";
        }

        @Override
        public final void match(@Nonnull Consumer<DeleteTask> deleteTask,
            @Nonnull Consumer<SaveTask> saveTask,
            @Nonnull Consumer<NotifyTaskMarkedComplete> notifyTaskMarkedComplete,
            @Nonnull Consumer<NotifyTaskMarkedActive> notifyTaskMarkedActive,
            @Nonnull Consumer<NotifyTaskSaveFailed> notifyTaskSaveFailed,
            @Nonnull Consumer<NotifyTaskDeletionFailed> notifyTaskDeletionFailed,
            @Nonnull Consumer<OpenTaskEditor> openTaskEditor, @Nonnull Consumer<Exit> exit) {
            notifyTaskMarkedComplete.accept(this);
        }

        @Override
        public final <R_> R_ map(@Nonnull Function<DeleteTask, R_> deleteTask,
            @Nonnull Function<SaveTask, R_> saveTask,
            @Nonnull Function<NotifyTaskMarkedComplete, R_> notifyTaskMarkedComplete,
            @Nonnull Function<NotifyTaskMarkedActive, R_> notifyTaskMarkedActive,
            @Nonnull Function<NotifyTaskSaveFailed, R_> notifyTaskSaveFailed,
            @Nonnull Function<NotifyTaskDeletionFailed, R_> notifyTaskDeletionFailed,
            @Nonnull Function<OpenTaskEditor, R_> openTaskEditor, @Nonnull Function<Exit, R_> exit) {
            return notifyTaskMarkedComplete.apply(this);
        }
    }

    /**
     * NotifyTaskMarkedActive
     */
    public static final class NotifyTaskMarkedActive extends TaskDetailEffect {
        NotifyTaskMarkedActive() {
        }

        @Override
        public boolean equals(Object other) {
            return other instanceof NotifyTaskMarkedActive;
        }

        @Override
        public int hashCode() {
            return 0;
        }

        @Override
        public String toString() {
            return "NotifyTaskMarkedActive{}";
        }

        @Override
        public final void match(@Nonnull Consumer<DeleteTask> deleteTask,
            @Nonnull Consumer<SaveTask> saveTask,
            @Nonnull Consumer<NotifyTaskMarkedComplete> notifyTaskMarkedComplete,
            @Nonnull Consumer<NotifyTaskMarkedActive> notifyTaskMarkedActive,
            @Nonnull Consumer<NotifyTaskSaveFailed> notifyTaskSaveFailed,
            @Nonnull Consumer<NotifyTaskDeletionFailed> notifyTaskDeletionFailed,
            @Nonnull Consumer<OpenTaskEditor> openTaskEditor, @Nonnull Consumer<Exit> exit) {
            notifyTaskMarkedActive.accept(this);
        }

        @Override
        public final <R_> R_ map(@Nonnull Function<DeleteTask, R_> deleteTask,
            @Nonnull Function<SaveTask, R_> saveTask,
            @Nonnull Function<NotifyTaskMarkedComplete, R_> notifyTaskMarkedComplete,
            @Nonnull Function<NotifyTaskMarkedActive, R_> notifyTaskMarkedActive,
            @Nonnull Function<NotifyTaskSaveFailed, R_> notifyTaskSaveFailed,
            @Nonnull Function<NotifyTaskDeletionFailed, R_> notifyTaskDeletionFailed,
            @Nonnull Function<OpenTaskEditor, R_> openTaskEditor, @Nonnull Function<Exit, R_> exit) {
            return notifyTaskMarkedActive.apply(this);
        }
    }

    /**
     * NotifyTaskSaveFailed
     */
    public static final class NotifyTaskSaveFailed extends TaskDetailEffect {
        NotifyTaskSaveFailed() {
        }

        @Override
        public boolean equals(Object other) {
            return other instanceof NotifyTaskSaveFailed;
        }

        @Override
        public int hashCode() {
            return 0;
        }

        @Override
        public String toString() {
            return "NotifyTaskSaveFailed{}";
        }

        @Override
        public final void match(@Nonnull Consumer<DeleteTask> deleteTask,
            @Nonnull Consumer<SaveTask> saveTask,
            @Nonnull Consumer<NotifyTaskMarkedComplete> notifyTaskMarkedComplete,
            @Nonnull Consumer<NotifyTaskMarkedActive> notifyTaskMarkedActive,
            @Nonnull Consumer<NotifyTaskSaveFailed> notifyTaskSaveFailed,
            @Nonnull Consumer<NotifyTaskDeletionFailed> notifyTaskDeletionFailed,
            @Nonnull Consumer<OpenTaskEditor> openTaskEditor, @Nonnull Consumer<Exit> exit) {
            notifyTaskSaveFailed.accept(this);
        }

        @Override
        public final <R_> R_ map(@Nonnull Function<DeleteTask, R_> deleteTask,
            @Nonnull Function<SaveTask, R_> saveTask,
            @Nonnull Function<NotifyTaskMarkedComplete, R_> notifyTaskMarkedComplete,
            @Nonnull Function<NotifyTaskMarkedActive, R_> notifyTaskMarkedActive,
            @Nonnull Function<NotifyTaskSaveFailed, R_> notifyTaskSaveFailed,
            @Nonnull Function<NotifyTaskDeletionFailed, R_> notifyTaskDeletionFailed,
            @Nonnull Function<OpenTaskEditor, R_> openTaskEditor, @Nonnull Function<Exit, R_> exit) {
            return notifyTaskSaveFailed.apply(this);
        }
    }

    /**
     * NotifyTaskDeletionFailed
     */
    public static final class NotifyTaskDeletionFailed extends TaskDetailEffect {
        NotifyTaskDeletionFailed() {
        }

        @Override
        public boolean equals(Object other) {
            return other instanceof NotifyTaskDeletionFailed;
        }

        @Override
        public int hashCode() {
            return 0;
        }

        @Override
        public String toString() {
            return "NotifyTaskDeletionFailed{}";
        }

        @Override
        public final void match(@Nonnull Consumer<DeleteTask> deleteTask,
            @Nonnull Consumer<SaveTask> saveTask,
            @Nonnull Consumer<NotifyTaskMarkedComplete> notifyTaskMarkedComplete,
            @Nonnull Consumer<NotifyTaskMarkedActive> notifyTaskMarkedActive,
            @Nonnull Consumer<NotifyTaskSaveFailed> notifyTaskSaveFailed,
            @Nonnull Consumer<NotifyTaskDeletionFailed> notifyTaskDeletionFailed,
            @Nonnull Consumer<OpenTaskEditor> openTaskEditor, @Nonnull Consumer<Exit> exit) {
            notifyTaskDeletionFailed.accept(this);
        }

        @Override
        public final <R_> R_ map(@Nonnull Function<DeleteTask, R_> deleteTask,
            @Nonnull Function<SaveTask, R_> saveTask,
            @Nonnull Function<NotifyTaskMarkedComplete, R_> notifyTaskMarkedComplete,
            @Nonnull Function<NotifyTaskMarkedActive, R_> notifyTaskMarkedActive,
            @Nonnull Function<NotifyTaskSaveFailed, R_> notifyTaskSaveFailed,
            @Nonnull Function<NotifyTaskDeletionFailed, R_> notifyTaskDeletionFailed,
            @Nonnull Function<OpenTaskEditor, R_> openTaskEditor, @Nonnull Function<Exit, R_> exit) {
            return notifyTaskDeletionFailed.apply(this);
        }
    }

    /**
     * OpenTaskEditor
     */
    public static final class OpenTaskEditor extends TaskDetailEffect {
        private final Task task;

        OpenTaskEditor(Task task) {
            this.task = checkNotNull(task);
        }

        /**
         * task
         *
         * @return Task
         */
        @Nonnull
        public final Task task() {
            return task;
        }

        @Override
        public boolean equals(Object other) {
            if (other == this) {
                return true;
            }
            if (!(other instanceof OpenTaskEditor)) {
                return false;
            }
            OpenTaskEditor o1 = (OpenTaskEditor) other;
            return o1.task.equals(this.task);
        }

        @Override
        public int hashCode() {
            int result = 0;
            return result * 31 + task.hashCode();
        }

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append("OpenTaskEditor{task=").append(task);
            return builder.append('}').toString();
        }

        @Override
        public final void match(@Nonnull Consumer<DeleteTask> deleteTask,
            @Nonnull Consumer<SaveTask> saveTask,
            @Nonnull Consumer<NotifyTaskMarkedComplete> notifyTaskMarkedComplete,
            @Nonnull Consumer<NotifyTaskMarkedActive> notifyTaskMarkedActive,
            @Nonnull Consumer<NotifyTaskSaveFailed> notifyTaskSaveFailed,
            @Nonnull Consumer<NotifyTaskDeletionFailed> notifyTaskDeletionFailed,
            @Nonnull Consumer<OpenTaskEditor> openTaskEditor, @Nonnull Consumer<Exit> exit) {
            openTaskEditor.accept(this);
        }

        @Override
        public final <R_> R_ map(@Nonnull Function<DeleteTask, R_> deleteTask,
            @Nonnull Function<SaveTask, R_> saveTask,
            @Nonnull Function<NotifyTaskMarkedComplete, R_> notifyTaskMarkedComplete,
            @Nonnull Function<NotifyTaskMarkedActive, R_> notifyTaskMarkedActive,
            @Nonnull Function<NotifyTaskSaveFailed, R_> notifyTaskSaveFailed,
            @Nonnull Function<NotifyTaskDeletionFailed, R_> notifyTaskDeletionFailed,
            @Nonnull Function<OpenTaskEditor, R_> openTaskEditor, @Nonnull Function<Exit, R_> exit) {
            return openTaskEditor.apply(this);
        }
    }

    /**
     * Exit
     */
    public static final class Exit extends TaskDetailEffect {
        Exit() {
        }

        @Override
        public boolean equals(Object other) {
            return other instanceof Exit;
        }

        @Override
        public int hashCode() {
            return 0;
        }

        @Override
        public String toString() {
            return "Exit{}";
        }

        @Override
        public final void match(@Nonnull Consumer<DeleteTask> deleteTask,
            @Nonnull Consumer<SaveTask> saveTask,
            @Nonnull Consumer<NotifyTaskMarkedComplete> notifyTaskMarkedComplete,
            @Nonnull Consumer<NotifyTaskMarkedActive> notifyTaskMarkedActive,
            @Nonnull Consumer<NotifyTaskSaveFailed> notifyTaskSaveFailed,
            @Nonnull Consumer<NotifyTaskDeletionFailed> notifyTaskDeletionFailed,
            @Nonnull Consumer<OpenTaskEditor> openTaskEditor, @Nonnull Consumer<Exit> exit) {
            exit.accept(this);
        }

        @Override
        public final <R_> R_ map(@Nonnull Function<DeleteTask, R_> deleteTask,
            @Nonnull Function<SaveTask, R_> saveTask,
            @Nonnull Function<NotifyTaskMarkedComplete, R_> notifyTaskMarkedComplete,
            @Nonnull Function<NotifyTaskMarkedActive, R_> notifyTaskMarkedActive,
            @Nonnull Function<NotifyTaskSaveFailed, R_> notifyTaskSaveFailed,
            @Nonnull Function<NotifyTaskDeletionFailed, R_> notifyTaskDeletionFailed,
            @Nonnull Function<OpenTaskEditor, R_> openTaskEditor, @Nonnull Function<Exit, R_> exit) {
            return exit.apply(this);
        }
    }
}
