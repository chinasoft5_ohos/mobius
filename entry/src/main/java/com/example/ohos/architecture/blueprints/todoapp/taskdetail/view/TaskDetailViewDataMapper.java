/*
 * -\-\-
 * --
 * Copyright (c) 2017-2018 Spotify AB
 * --
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * -/-/-
 */

package com.example.ohos.architecture.blueprints.todoapp.taskdetail.view;

import ohos.agp.components.Component;

import com.example.ohos.architecture.blueprints.todoapp.data.Task;
import com.example.ohos.architecture.blueprints.todoapp.data.TaskDetails;

import static com.google.common.base.Strings.isNullOrEmpty;

/**
 * Maps instances of {@link Task} to {@link TaskDetailViewData}
 */
public class TaskDetailViewDataMapper {
    /**
     * taskToTaskViewData
     *
     * @param task task
     * @return TaskDetailViewData
     */
    public static TaskDetailViewData taskToTaskViewData(Task task) {
        TaskDetails details = task.details();
        String title = details.title();
        String description = details.description();

        return TaskDetailViewData.builder()
            .title(TaskDetailViewData.TextViewData.create(isNullOrEmpty(title) ? Component.HIDE : Component.VISIBLE, title))
            .description(
                TaskDetailViewData.TextViewData.create(isNullOrEmpty(description) ? Component.HIDE : Component.VISIBLE, description))
            .completedChecked(details.completed())
            .build();
    }
}
