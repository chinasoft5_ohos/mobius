/*
 * -\-\-
 * --
 * Copyright (c) 2017-2018 Spotify AB
 * --
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * -/-/-
 */

package com.example.ohos.architecture.blueprints.todoapp.tasks.view;


import ohos.agp.components.Component;

import com.example.ohos.architecture.blueprints.todoapp.tasks.domain.TasksFilterType;
import com.ohos.mobius.ResourceTable;

/**
 * EmptyTasksViewDataMapper
 */
public class EmptyTasksViewDataMapper {
    /**
     * createEmptyTaskViewData
     *
     * @param filter filter
     * @return TasksListViewData.EmptyTasksViewData
     */
    public static TasksListViewData.EmptyTasksViewData createEmptyTaskViewData(TasksFilterType filter) {
        TasksListViewData.EmptyTasksViewData.Builder builder = new TasksListViewData.EmptyTasksViewData.Builder();
        switch (filter) {
            case ACTIVE_TASKS:
                return builder
                    .addViewVisibility(Component.HIDE)
                    .title(ResourceTable.String_no_tasks_active)
                    .icon(ResourceTable.Media_icon)
                    .build();
            case COMPLETED_TASKS:
                return builder
                    .addViewVisibility(Component.HIDE)
                    .title(ResourceTable.String_no_tasks_completed)
                    .icon(ResourceTable.Media_icon)
                    .build();
            default:
                return builder
                    .addViewVisibility(Component.VISIBLE)
                    .title(ResourceTable.String_no_tasks_all)
                    .icon(ResourceTable.Media_icon)
                    .build();
        }
    }
}
