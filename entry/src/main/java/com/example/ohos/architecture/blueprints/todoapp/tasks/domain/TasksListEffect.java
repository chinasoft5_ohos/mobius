package com.example.ohos.architecture.blueprints.todoapp.tasks.domain;

import static com.spotify.dataenum.DataenumUtils.checkNotNull;

import com.example.ohos.architecture.blueprints.todoapp.data.Task;
import com.google.common.collect.ImmutableList;
import com.spotify.dataenum.function.Consumer;
import com.spotify.dataenum.function.Function;

import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.StringBuilder;

import javax.annotation.Generated;
import javax.annotation.Nonnull;

/**
 * TasksListEffect
 */
@Generated("com.spotify.dataenum.processor.DataEnumProcessor")
public abstract class TasksListEffect {
    TasksListEffect() {
    }

    /**
     * refreshTasks
     *
     * @return TasksListEffect
     */
    public static TasksListEffect refreshTasks() {
        return new RefreshTasks();
    }

    /**
     * loadTasks
     *
     * @return TasksListEffect
     */
    public static TasksListEffect loadTasks() {
        return new LoadTasks();
    }

    /**
     * saveTask
     *
     * @param task task
     * @return TasksListEffect
     */
    public static TasksListEffect saveTask(@Nonnull Task task) {
        return new SaveTask(task);
    }

    /**
     * deleteTasks
     *
     * @param tasks tasks
     * @return TasksListEffect
     */
    public static TasksListEffect deleteTasks(@Nonnull ImmutableList<Task> tasks) {
        return new DeleteTasks(tasks);
    }

    /**
     * showFeedback
     *
     * @param feedbackType feedbackType
     * @return TasksListEffect
     */
    public static TasksListEffect showFeedback(@Nonnull FeedbackType feedbackType) {
        return new ShowFeedback(feedbackType);
    }

    /**
     * navigateToTaskDetails
     *
     * @param task task
     * @return TasksListEffect
     */
    public static TasksListEffect navigateToTaskDetails(@Nonnull Task task) {
        return new NavigateToTaskDetails(task);
    }

    /**
     * startTaskCreationFlow
     *
     * @return TasksListEffect
     */
    public static TasksListEffect startTaskCreationFlow() {
        return new StartTaskCreationFlow();
    }

    /**
     * isRefreshTasks
     *
     * @return boolean
     */
    public final boolean isRefreshTasks() {
        return (this instanceof RefreshTasks);
    }

    /**
     * isLoadTasks
     *
     * @return boolean
     */
    public final boolean isLoadTasks() {
        return (this instanceof LoadTasks);
    }

    /**
     * isSaveTask
     *
     * @return boolean
     */
    public final boolean isSaveTask() {
        return (this instanceof SaveTask);
    }

    /**
     * isDeleteTasks
     *
     * @return boolean
     */
    public final boolean isDeleteTasks() {
        return (this instanceof DeleteTasks);
    }

    /**
     * isShowFeedback
     *
     * @return boolean
     */
    public final boolean isShowFeedback() {
        return (this instanceof ShowFeedback);
    }

    /**
     * isNavigateToTaskDetails
     *
     * @return boolean
     */
    public final boolean isNavigateToTaskDetails() {
        return (this instanceof NavigateToTaskDetails);
    }

    /**
     * isStartTaskCreationFlow
     *
     * @return boolean
     */
    public final boolean isStartTaskCreationFlow() {
        return (this instanceof StartTaskCreationFlow);
    }

    /**
     * asRefreshTasks
     *
     * @return RefreshTasks
     */
    public final RefreshTasks asRefreshTasks() {
        return (RefreshTasks) this;
    }

    /**
     * asLoadTasks
     *
     * @return LoadTasks
     */
    public final LoadTasks asLoadTasks() {
        return (LoadTasks) this;
    }

    /**
     * asSaveTask
     *
     * @return SaveTask
     */
    public final SaveTask asSaveTask() {
        return (SaveTask) this;
    }

    /**
     * asDeleteTasks
     *
     * @return DeleteTasks
     */
    public final DeleteTasks asDeleteTasks() {
        return (DeleteTasks) this;
    }

    /**
     * asShowFeedback
     *
     * @return ShowFeedback
     */
    public final ShowFeedback asShowFeedback() {
        return (ShowFeedback) this;
    }

    /**
     * asNavigateToTaskDetails
     *
     * @return NavigateToTaskDetails
     */
    public final NavigateToTaskDetails asNavigateToTaskDetails() {
        return (NavigateToTaskDetails) this;
    }

    /**
     * asStartTaskCreationFlow
     *
     * @return StartTaskCreationFlow
     */
    public final StartTaskCreationFlow asStartTaskCreationFlow() {
        return (StartTaskCreationFlow) this;
    }

    /**
     * match
     *
     * @param refreshTasks refreshTasks
     * @param loadTasks loadTasks
     * @param saveTask saveTask
     * @param deleteTasks deleteTasks
     * @param showFeedback showFeedback
     * @param navigateToTaskDetails navigateToTaskDetails
     * @param startTaskCreationFlow startTaskCreationFlow
     */
    public abstract void match(@Nonnull Consumer<RefreshTasks> refreshTasks,
    @Nonnull Consumer<LoadTasks> loadTasks, @Nonnull Consumer<SaveTask> saveTask,
    @Nonnull Consumer<DeleteTasks> deleteTasks, @Nonnull Consumer<ShowFeedback> showFeedback,
    @Nonnull Consumer<NavigateToTaskDetails> navigateToTaskDetails,
    @Nonnull Consumer<StartTaskCreationFlow> startTaskCreationFlow);

    /**
     * map
     *
     * @param refreshTasks refreshTasks
     * @param loadTasks loadTasks
     * @param saveTask saveTask
     * @param deleteTasks deleteTasks
     * @param showFeedback showFeedback
     * @param navigateToTaskDetails navigateToTaskDetails
     * @param startTaskCreationFlow startTaskCreationFlow
     * @param <R_> R_
     * @return R_
     */
    public abstract <R_> R_ map(@Nonnull Function<RefreshTasks, R_> refreshTasks,
    @Nonnull Function<LoadTasks, R_> loadTasks, @Nonnull Function<SaveTask, R_> saveTask,
    @Nonnull Function<DeleteTasks, R_> deleteTasks,
    @Nonnull Function<ShowFeedback, R_> showFeedback,
    @Nonnull Function<NavigateToTaskDetails, R_> navigateToTaskDetails,
    @Nonnull Function<StartTaskCreationFlow, R_> startTaskCreationFlow);

    /**
     * RefreshTasks
     */
    public static final class RefreshTasks extends TasksListEffect {
        RefreshTasks() {
        }

        @Override
        public boolean equals(Object other) {
            return other instanceof RefreshTasks;
        }

        @Override
        public int hashCode() {
            return 0;
        }

        @Override
        public String toString() {
            return "RefreshTasks{}";
        }

        @Override
        public final void match(@Nonnull Consumer<RefreshTasks> refreshTasks,
            @Nonnull Consumer<LoadTasks> loadTasks, @Nonnull Consumer<SaveTask> saveTask,
            @Nonnull Consumer<DeleteTasks> deleteTasks, @Nonnull Consumer<ShowFeedback> showFeedback,
            @Nonnull Consumer<NavigateToTaskDetails> navigateToTaskDetails,
            @Nonnull Consumer<StartTaskCreationFlow> startTaskCreationFlow) {
            refreshTasks.accept(this);
        }

        @Override
        public final <R_> R_ map(@Nonnull Function<RefreshTasks, R_> refreshTasks,
            @Nonnull Function<LoadTasks, R_> loadTasks, @Nonnull Function<SaveTask, R_> saveTask,
            @Nonnull Function<DeleteTasks, R_> deleteTasks,
            @Nonnull Function<ShowFeedback, R_> showFeedback,
            @Nonnull Function<NavigateToTaskDetails, R_> navigateToTaskDetails,
            @Nonnull Function<StartTaskCreationFlow, R_> startTaskCreationFlow) {
            return refreshTasks.apply(this);
        }
    }

    /**
     * LoadTasks
     */
    public static final class LoadTasks extends TasksListEffect {
        LoadTasks() {
        }

        @Override
        public boolean equals(Object other) {
            return other instanceof LoadTasks;
        }

        @Override
        public int hashCode() {
            return 0;
        }

        @Override
        public String toString() {
            return "LoadTasks{}";
        }

        @Override
        public final void match(@Nonnull Consumer<RefreshTasks> refreshTasks,
            @Nonnull Consumer<LoadTasks> loadTasks, @Nonnull Consumer<SaveTask> saveTask,
            @Nonnull Consumer<DeleteTasks> deleteTasks, @Nonnull Consumer<ShowFeedback> showFeedback,
            @Nonnull Consumer<NavigateToTaskDetails> navigateToTaskDetails,
            @Nonnull Consumer<StartTaskCreationFlow> startTaskCreationFlow) {
            loadTasks.accept(this);
        }

        @Override
        public final <R_> R_ map(@Nonnull Function<RefreshTasks, R_> refreshTasks,
            @Nonnull Function<LoadTasks, R_> loadTasks, @Nonnull Function<SaveTask, R_> saveTask,
            @Nonnull Function<DeleteTasks, R_> deleteTasks,
            @Nonnull Function<ShowFeedback, R_> showFeedback,
            @Nonnull Function<NavigateToTaskDetails, R_> navigateToTaskDetails,
            @Nonnull Function<StartTaskCreationFlow, R_> startTaskCreationFlow) {
            return loadTasks.apply(this);
        }
    }

    /**
     * SaveTask
     */
    public static final class SaveTask extends TasksListEffect {
        private final Task task;

        SaveTask(Task task) {
            this.task = checkNotNull(task);
        }

        @Nonnull
        public final Task task() {
            return task;
        }

        @Override
        public boolean equals(Object other) {
            if (other == this) {
                return true;
            }
            if (!(other instanceof SaveTask)) {
                return false;
            }
            SaveTask o1 = (SaveTask) other;
            return o1.task.equals(this.task);
        }

        @Override
        public int hashCode() {
            int result = 0;
            return result * 31 + task.hashCode();
        }

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append("SaveTask{task=").append(task);
            return builder.append('}').toString();
        }

        @Override
        public final void match(@Nonnull Consumer<RefreshTasks> refreshTasks,
            @Nonnull Consumer<LoadTasks> loadTasks, @Nonnull Consumer<SaveTask> saveTask,
            @Nonnull Consumer<DeleteTasks> deleteTasks, @Nonnull Consumer<ShowFeedback> showFeedback,
            @Nonnull Consumer<NavigateToTaskDetails> navigateToTaskDetails,
            @Nonnull Consumer<StartTaskCreationFlow> startTaskCreationFlow) {
            saveTask.accept(this);
        }

        @Override
        public final <R_> R_ map(@Nonnull Function<RefreshTasks, R_> refreshTasks,
            @Nonnull Function<LoadTasks, R_> loadTasks, @Nonnull Function<SaveTask, R_> saveTask,
            @Nonnull Function<DeleteTasks, R_> deleteTasks,
            @Nonnull Function<ShowFeedback, R_> showFeedback,
            @Nonnull Function<NavigateToTaskDetails, R_> navigateToTaskDetails,
            @Nonnull Function<StartTaskCreationFlow, R_> startTaskCreationFlow) {
            return saveTask.apply(this);
        }
    }

    /**
     * DeleteTasks
     */
    public static final class DeleteTasks extends TasksListEffect {
        private final ImmutableList<Task> tasks;

        DeleteTasks(ImmutableList<Task> tasks) {
            this.tasks = checkNotNull(tasks);
        }

        /**
         * tasks
         *
         * @return ImmutableList<Task>
         */
        @Nonnull
        public final ImmutableList<Task> tasks() {
            return tasks;
        }

        @Override
        public boolean equals(Object other) {
            if (other == this) {
                return true;
            }
            if (!(other instanceof DeleteTasks)) {
                return false;
            }
            DeleteTasks o1 = (DeleteTasks) other;
            return o1.tasks.equals(this.tasks);
        }

        @Override
        public int hashCode() {
            int result = 0;
            return result * 31 + tasks.hashCode();
        }

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append("DeleteTasks{tasks=").append(tasks);
            return builder.append('}').toString();
        }

        @Override
        public final void match(@Nonnull Consumer<RefreshTasks> refreshTasks,
            @Nonnull Consumer<LoadTasks> loadTasks, @Nonnull Consumer<SaveTask> saveTask,
            @Nonnull Consumer<DeleteTasks> deleteTasks, @Nonnull Consumer<ShowFeedback> showFeedback,
            @Nonnull Consumer<NavigateToTaskDetails> navigateToTaskDetails,
            @Nonnull Consumer<StartTaskCreationFlow> startTaskCreationFlow) {
            deleteTasks.accept(this);
        }

        @Override
        public final <R_> R_ map(@Nonnull Function<RefreshTasks, R_> refreshTasks,
            @Nonnull Function<LoadTasks, R_> loadTasks, @Nonnull Function<SaveTask, R_> saveTask,
            @Nonnull Function<DeleteTasks, R_> deleteTasks,
            @Nonnull Function<ShowFeedback, R_> showFeedback,
            @Nonnull Function<NavigateToTaskDetails, R_> navigateToTaskDetails,
            @Nonnull Function<StartTaskCreationFlow, R_> startTaskCreationFlow) {
            return deleteTasks.apply(this);
        }
    }

    /**
     * ShowFeedback
     */
    public static final class ShowFeedback extends TasksListEffect {
        private final FeedbackType feedbackType;

        ShowFeedback(FeedbackType feedbackType) {
            this.feedbackType = checkNotNull(feedbackType);
        }

        @Nonnull
        public final FeedbackType feedbackType() {
            return feedbackType;
        }

        @Override
        public boolean equals(Object other) {
            if (other == this) {
                return true;
            }
            if (!(other instanceof ShowFeedback)) {
                return false;
            }
            ShowFeedback o1 = (ShowFeedback) other;
            return o1.feedbackType == feedbackType;
        }

        @Override
        public int hashCode() {
            int result = 0;
            return result * 31 + feedbackType.hashCode();
        }

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append("ShowFeedback{feedbackType=").append(feedbackType);
            return builder.append('}').toString();
        }

        @Override
        public final void match(@Nonnull Consumer<RefreshTasks> refreshTasks,
            @Nonnull Consumer<LoadTasks> loadTasks, @Nonnull Consumer<SaveTask> saveTask,
            @Nonnull Consumer<DeleteTasks> deleteTasks, @Nonnull Consumer<ShowFeedback> showFeedback,
            @Nonnull Consumer<NavigateToTaskDetails> navigateToTaskDetails,
            @Nonnull Consumer<StartTaskCreationFlow> startTaskCreationFlow) {
            showFeedback.accept(this);
        }

        @Override
        public final <R_> R_ map(@Nonnull Function<RefreshTasks, R_> refreshTasks,
            @Nonnull Function<LoadTasks, R_> loadTasks, @Nonnull Function<SaveTask, R_> saveTask,
            @Nonnull Function<DeleteTasks, R_> deleteTasks,
            @Nonnull Function<ShowFeedback, R_> showFeedback,
            @Nonnull Function<NavigateToTaskDetails, R_> navigateToTaskDetails,
            @Nonnull Function<StartTaskCreationFlow, R_> startTaskCreationFlow) {
            return showFeedback.apply(this);
        }
    }

    /**
     * NavigateToTaskDetails
     */
    public static final class NavigateToTaskDetails extends TasksListEffect {
        private final Task task;

        NavigateToTaskDetails(Task task) {
            this.task = checkNotNull(task);
        }

        /**
         * task
         *
         * @return Task
         */
        @Nonnull
        public final Task task() {
            return task;
        }

        @Override
        public boolean equals(Object other) {
            if (other == this) {
                return true;
            }
            if (!(other instanceof NavigateToTaskDetails)) {
                return false;
            }
            NavigateToTaskDetails o1 = (NavigateToTaskDetails) other;
            return o1.task.equals(this.task);
        }

        @Override
        public int hashCode() {
            int result = 0;
            return result * 31 + task.hashCode();
        }

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append("NavigateToTaskDetails{task=").append(task);
            return builder.append('}').toString();
        }

        @Override
        public final void match(@Nonnull Consumer<RefreshTasks> refreshTasks,
            @Nonnull Consumer<LoadTasks> loadTasks, @Nonnull Consumer<SaveTask> saveTask,
            @Nonnull Consumer<DeleteTasks> deleteTasks, @Nonnull Consumer<ShowFeedback> showFeedback,
            @Nonnull Consumer<NavigateToTaskDetails> navigateToTaskDetails,
            @Nonnull Consumer<StartTaskCreationFlow> startTaskCreationFlow) {
            navigateToTaskDetails.accept(this);
        }

        @Override
        public final <R_> R_ map(@Nonnull Function<RefreshTasks, R_> refreshTasks,
            @Nonnull Function<LoadTasks, R_> loadTasks, @Nonnull Function<SaveTask, R_> saveTask,
            @Nonnull Function<DeleteTasks, R_> deleteTasks,
            @Nonnull Function<ShowFeedback, R_> showFeedback,
            @Nonnull Function<NavigateToTaskDetails, R_> navigateToTaskDetails,
            @Nonnull Function<StartTaskCreationFlow, R_> startTaskCreationFlow) {
            return navigateToTaskDetails.apply(this);
        }
    }

    /**
     * StartTaskCreationFlow
     */
    public static final class StartTaskCreationFlow extends TasksListEffect {
        StartTaskCreationFlow() {
        }

        @Override
        public boolean equals(Object other) {
            return other instanceof StartTaskCreationFlow;
        }

        @Override
        public int hashCode() {
            return 0;
        }

        @Override
        public String toString() {
            return "StartTaskCreationFlow{}";
        }

        @Override
        public final void match(@Nonnull Consumer<RefreshTasks> refreshTasks,
            @Nonnull Consumer<LoadTasks> loadTasks, @Nonnull Consumer<SaveTask> saveTask,
            @Nonnull Consumer<DeleteTasks> deleteTasks, @Nonnull Consumer<ShowFeedback> showFeedback,
            @Nonnull Consumer<NavigateToTaskDetails> navigateToTaskDetails,
            @Nonnull Consumer<StartTaskCreationFlow> startTaskCreationFlow) {
            startTaskCreationFlow.accept(this);
        }

        @Override
        public final <R_> R_ map(@Nonnull Function<RefreshTasks, R_> refreshTasks,
            @Nonnull Function<LoadTasks, R_> loadTasks, @Nonnull Function<SaveTask, R_> saveTask,
            @Nonnull Function<DeleteTasks, R_> deleteTasks,
            @Nonnull Function<ShowFeedback, R_> showFeedback,
            @Nonnull Function<NavigateToTaskDetails, R_> navigateToTaskDetails,
            @Nonnull Function<StartTaskCreationFlow, R_> startTaskCreationFlow) {
            return startTaskCreationFlow.apply(this);
        }
    }
}
