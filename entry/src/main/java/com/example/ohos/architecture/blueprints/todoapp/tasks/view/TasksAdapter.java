/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.ohos.architecture.blueprints.todoapp.tasks.view;

import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Checkbox;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;

import com.example.ohos.architecture.blueprints.todoapp.util.LogUtil;
import com.google.common.collect.ImmutableList;
import com.ohos.mobius.ResourceTable;

import static com.google.common.base.Preconditions.checkNotNull;

class TasksAdapter extends BaseItemProvider {
    private ImmutableList<TasksListViewData.TaskViewData> mTasks;
    private TaskItemListener mItemListener;

    /**
     * setItemListener
     *
     * @param itemListener itemListener
     */
    public void setItemListener(TaskItemListener itemListener) {
        mItemListener = itemListener;
    }

    /**
     * replaceData
     *
     * @param tasks tasks
     */
    public void replaceData(ImmutableList<TasksListViewData.TaskViewData> tasks) {
        mTasks = checkNotNull(tasks);
        notifyDataChanged();
    }

    @Override
    public int getCount() {
        return mTasks == null ? 0 : mTasks.size();
    }

    @Override
    public TasksListViewData.TaskViewData getItem(int i1) {
        return mTasks.get(i1);
    }

    @Override
    public long getItemId(int i1) {
        return i1;
    }

    /**
     * getComponent
     *
     * @param po po
     * @param view view
     * @param viewGroup viewGroup
     * @return Component
     */
    @Override
    public Component getComponent(int po, Component view, ComponentContainer viewGroup) {
        Component rowView = view;
        if (rowView == null) {
            LayoutScatter inflater = LayoutScatter.getInstance(viewGroup.getContext());
            rowView = inflater.parse(ResourceTable.Layout_task_item, viewGroup, false);
        }

        final TasksListViewData.TaskViewData task = getItem(po);
        Text titleTV = (Text) rowView.findComponentById(ResourceTable.Id_title);
        titleTV.setText(task.title());

        Checkbox completeCB = (Checkbox) rowView.findComponentById(ResourceTable.Id_complete);
        completeCB.setChecked(task.completed());

        completeCB.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (mItemListener == null) {
                    return;
                }

                if (!task.completed()) {
                    mItemListener.onCompleteTaskClick(task.id());
                } else {
                    mItemListener.onActivateTaskClick(task.id());
                }
            }
        });
        rowView.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                LogUtil.debug("MOBIUSTag_", "task:" + task.id());
                if (mItemListener != null) {
                    mItemListener.onTaskClick(task.id());
                }
            }
        });
        return rowView;
    }

    /**
     * TaskItemListener
     */
    public interface TaskItemListener {
        void onTaskClick(String id);

        void onCompleteTaskClick(String id);

        void onActivateTaskClick(String id);
    }
}
