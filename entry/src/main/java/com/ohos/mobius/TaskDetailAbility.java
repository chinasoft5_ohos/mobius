package com.ohos.mobius;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.app.Context;

import com.example.ohos.architecture.blueprints.todoapp.data.Task;
import com.ohos.mobius.slice.TaskDetailAbilitySlice;

/**
 * TaskDetailAbility
 */
public class TaskDetailAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(TaskDetailAbilitySlice.class.getName());
    }

    /**
     * getIntent
     *
     * @param context context
     * @param toClass toClass
     * @param task task
     * @return Intent
     */
    public static Intent getIntent(Context context, Class toClass, Task task) {
        if (context == null) {
            throw new NullPointerException("you are context is null");
        }
        Intent intent = new Intent();
        intent.setParam("taskob", task);
        intent.setOperation(new Intent.OperationBuilder()
            .withBundleName(context.getBundleName())
            .withAbilityName(toClass.getName())
            .build());
        return intent;
    }
}
