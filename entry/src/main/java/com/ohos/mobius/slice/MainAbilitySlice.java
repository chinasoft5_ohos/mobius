package com.ohos.mobius.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Image;

import com.example.ohos.architecture.blueprints.todoapp.data.Task;
import com.example.ohos.architecture.blueprints.todoapp.tasks.TasksInjector;
import com.example.ohos.architecture.blueprints.todoapp.tasks.domain.TasksListEvent;
import com.example.ohos.architecture.blueprints.todoapp.tasks.domain.TasksListModel;
import com.example.ohos.architecture.blueprints.todoapp.tasks.view.DeferredEventSource;
import com.example.ohos.architecture.blueprints.todoapp.tasks.view.TasksListViewDataMapper;
import com.example.ohos.architecture.blueprints.todoapp.tasks.view.TasksViews;
import com.ohos.mobius.AddEditTaskAbility;
import com.ohos.mobius.ResourceTable;
import com.ohos.mobius.TaskDetailAbility;
import com.spotify.mobius.MobiusLoop;

import io.reactivex.subjects.PublishSubject;

import static com.example.ohos.architecture.blueprints.todoapp.tasks.domain.TasksListEvent.taskCreated;
import static com.example.ohos.architecture.blueprints.todoapp.tasks.effecthandlers.TasksListEffectHandlers.createEffectHandler;
import static com.spotify.mobius.extras.Connectables.contramap;

/**
 * MainAbilitySlice
 */
public class MainAbilitySlice extends AbilitySlice {
    private MobiusLoop.Controller<TasksListModel, TasksListEvent> mController;
    private PublishSubject<TasksListEvent> mMenuEvents = PublishSubject.create();
    private TasksViews mViews;
    private DeferredEventSource<TasksListEvent> mEventSource = new DeferredEventSource<>();
    long lastTime;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        getWindow().setStatusBarColor(getColor(ResourceTable.Color_colorTitle));
        super.setUIContent(ResourceTable.Layout_tasks_frag);
        Image imageAdd = (Image) findComponentById(ResourceTable.Id_image_add);
        mViews = new TasksViews(getContext(), this, imageAdd, mMenuEvents);
        mController =
            TasksInjector.createController(
                createEffectHandler(getContext(), mViews, this::showAddTask, this::showTaskDetailsUi),
                mEventSource,
                resolveDefaultModel());

        mController.connect(contramap(TasksListViewDataMapper::tasksListModelToViewData, mViews));
    }

    private TasksListModel resolveDefaultModel() {
        return TasksListModel.DEFAULT;
    }

    @Override
    public void onActive() {
        super.onActive();
        mController.start();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
        mController.stop();
    }

    /**
     * showAddTask
     */
    public void showAddTask() {
        startAbilityForResult(AddEditTaskAbility.getIntent(getContext(), AddEditTaskAbility.class), AddEditTaskAbility.REQUEST_ADD_TASK);
    }

    /**
     * showTaskDetailsUi
     * @param task task
     */
    public void showTaskDetailsUi(Task task) {
        if (onDoubClick()) {
            startAbility(TaskDetailAbility.getIntent(getContext(), TaskDetailAbility.class, task));
        }
    }


    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
        if (resultCode == AddEditTaskAbility.RESULT_ADD_TASK) {
            mEventSource.notifyEvent(taskCreated());
        }
    }

    private boolean onDoubClick() {
        boolean isFlag = false;
        long time = System.currentTimeMillis() - lastTime;

        if (time > 1500) {
            isFlag = true;
            lastTime = System.currentTimeMillis();
        }
        return isFlag;
    }

}
