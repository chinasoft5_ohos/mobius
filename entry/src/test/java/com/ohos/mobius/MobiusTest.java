package com.ohos.mobius;

import com.example.ohos.architecture.blueprints.todoapp.util.LogUtil;
import com.spotify.mobius.Connection;
import com.spotify.mobius.Effects;
import com.spotify.mobius.Mobius;
import com.spotify.mobius.MobiusLoop;
import com.spotify.mobius.Next;
import com.spotify.mobius.functions.Consumer;

import org.junit.Test;

/**
 * @since 2021-07-12
 */
public class MobiusTest {
    private MobiusLoop<Integer, Event, Effect> loop;
    enum Event {
        UP,
        DOWN
    }

    enum Effect {
        REPORT_ERROR_NEGATIVE
    }

    /**
     * MobiusloopTest
     */
    @Test
    public void MobiusloopTest() {
        if (loop == null) {
            loop = Mobius.loop(MobiusTest::update, MobiusTest::effectHandler).startFrom(3);
        }
        loop.dispatchEvent(Event.DOWN);    // prints "2"
        loop.dispatchEvent(Event.DOWN);    // prints "1"
        loop.dispatchEvent(Event.DOWN);    // prints "0"
        loop.dispatchEvent(Event.DOWN);    // prints "0"
        loop.dispatchEvent(Event.UP);      // prints "1"
        loop.dispatchEvent(Event.UP);      // prints "2"
        loop.dispatchEvent(Event.DOWN);    // prints "1"
        loop.observe(counter -> LogUtil.debug("TestPrint",""+counter));
    }

    static Next<Integer, Effect> update(int counter, Event event) {
        switch (event) {
            case UP:
                return Next.next(counter + 1);

            case DOWN: {
                if (counter > 0) {
                    return Next.next(counter - 1);
                }
                return Next.dispatch(Effects.effects(Effect.REPORT_ERROR_NEGATIVE));
            }
        }
        return Next.next(0);
    }

    static Connection<Effect> effectHandler(Consumer<Event> eventConsumer) {
        return new Connection<Effect>() {
            @Override
            public void accept(Effect effect) {
                if (effect == Effect.REPORT_ERROR_NEGATIVE) {
                    LogUtil.debug("TestPrint","error!");
                }
            }

            @Override
            public void dispose() {
                // We don't have any resources to release, so we can leave this empty.
            }
        };
    }
}
