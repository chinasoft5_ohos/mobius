package com.ohos.mobius;

import ohos.aafwk.ability.AbilityPackage;

import com.example.ohos.architecture.blueprints.todoapp.util.PreferencesUtil;

/**
 * MyApplication
 */
public class MyApplication extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
        PreferencesUtil.init(getApplicationContext());
    }
}
