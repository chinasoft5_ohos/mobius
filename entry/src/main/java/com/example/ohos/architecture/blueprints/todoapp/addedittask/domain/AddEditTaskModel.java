/*
 * -\-\-
 * --
 * Copyright (c) 2017-2018 Spotify AB
 * --
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * -/-/-
 */

package com.example.ohos.architecture.blueprints.todoapp.addedittask.domain;

import com.example.ohos.architecture.blueprints.todoapp.data.TaskDetails;
import com.google.auto.value.AutoValue;

/**
 * @since 2021-06-08
 */
public class AddEditTaskModel {
    /**
     * withDetails
     *
     * @param details details
     * @return AddEditTaskModel
     */
    public AddEditTaskModel withDetails(TaskDetails details) {
        return toBuilder().details(details).build();
    }

    /**
     * builder
     *
     * @return Builder
     */
    public static Builder builder() {
        return new AddEditTaskModel.Builder();
    }

    private final AddEditTaskMode mode;
    private final TaskDetails details;

    private AddEditTaskModel(
        AddEditTaskMode mode,
        TaskDetails details) {
        this.mode = mode;
        this.details = details;
    }

    /**
     * mode
     *
     * @return AddEditTaskMode
     */
    public AddEditTaskMode mode() {
        return mode;
    }

    /**
     * details
     *
     * @return TaskDetails
     */
    public TaskDetails details() {
        return details;
    }

    @Override
    public String toString() {
        return "AddEditTaskModel{"
            + "mode=" + mode + ", "
            + "details=" + details
            + "}";
    }

    /**
     * toBuilder
     *
     * @return Builder Builder
     */
    public AddEditTaskModel.Builder toBuilder() {
        return new Builder(this);
    }

    /**
     * Builder
     */
    public static final class Builder {
        private AddEditTaskMode mode;
        private TaskDetails details;

        Builder() {
        }

        /**
         * Builder
         *
         * @param source source
         */
        private Builder(AddEditTaskModel source) {
            this.mode = source.mode();
            this.details = source.details();
        }

        /**
         * mode
         *
         * @param mode mode
         * @return Builder
         */
        public AddEditTaskModel.Builder mode(AddEditTaskMode mode) {
            if (mode == null) {
                throw new NullPointerException("Null mode");
            }
            this.mode = mode;
            return this;
        }

        /**
         * details
         *
         * @param details details
         * @return Builder Builder
         */
        public AddEditTaskModel.Builder details(TaskDetails details) {
            if (details == null) {
                throw new NullPointerException("Null details");
            }
            this.details = details;
            return this;
        }

        /**
         * build
         *
         * @return AddEditTaskModel
         */
        public AddEditTaskModel build() {
            String missing = "";
            if (this.mode == null) {
                missing += " mode";
            }
            if (this.details == null) {
                missing += " details";
            }
            if (!missing.isEmpty()) {
                throw new IllegalStateException("Missing required properties:" + missing);
            }
            return new AddEditTaskModel(this.mode, this.details);
        }
    }
}
