package com.example.ohos.architecture.blueprints.todoapp.taskdetail.domain;

import com.spotify.dataenum.function.Consumer;
import com.spotify.dataenum.function.Function;

import javax.annotation.Generated;
import javax.annotation.Nonnull;

/**
 * TaskDetailEvent
 */
@Generated("com.spotify.dataenum.processor.DataEnumProcessor")
public abstract class TaskDetailEvent {
    TaskDetailEvent() {
    }

    /**
     * deleteTaskRequested
     *
     * @return TaskDetailEvent
     */
    public static TaskDetailEvent deleteTaskRequested() {
        return new DeleteTaskRequested();
    }

    /**
     * completeTaskRequested
     *
     * @return TaskDetailEvent
     */
    public static TaskDetailEvent completeTaskRequested() {
        return new CompleteTaskRequested();
    }

    /**
     * activateTaskRequested
     *
     * @return TaskDetailEvent
     */
    public static TaskDetailEvent activateTaskRequested() {
        return new ActivateTaskRequested();
    }

    /**
     * editTaskRequested
     *
     * @return TaskDetailEvent
     */
    public static TaskDetailEvent editTaskRequested() {
        return new EditTaskRequested();
    }

    /**
     * taskDeleted
     *
     * @return TaskDetailEvent
     */
    public static TaskDetailEvent taskDeleted() {
        return new TaskDeleted();
    }

    /**
     * taskMarkedComplete
     *
     * @return TaskDetailEvent
     */
    public static TaskDetailEvent taskMarkedComplete() {
        return new TaskMarkedComplete();
    }

    /**
     * taskMarkedActive
     *
     * @return TaskDetailEvent
     */
    public static TaskDetailEvent taskMarkedActive() {
        return new TaskMarkedActive();
    }

    /**
     * taskSaveFailed
     *
     * @return TaskDetailEvent
     */
    public static TaskDetailEvent taskSaveFailed() {
        return new TaskSaveFailed();
    }

    /**
     * taskDeletionFailed
     *
     * @return TaskDetailEvent
     */
    public static TaskDetailEvent taskDeletionFailed() {
        return new TaskDeletionFailed();
    }

    /**
     * isDeleteTaskRequested
     *
     * @return boolean
     */
    public final boolean isDeleteTaskRequested() {
        return (this instanceof DeleteTaskRequested);
    }

    /**
     * isCompleteTaskRequested
     *
     * @return boolean
     */
    public final boolean isCompleteTaskRequested() {
        return (this instanceof CompleteTaskRequested);
    }

    /**
     * isActivateTaskRequested
     *
     * @return boolean
     */
    public final boolean isActivateTaskRequested() {
        return (this instanceof ActivateTaskRequested);
    }

    /**
     * isEditTaskRequested
     *
     * @return boolean
     */
    public final boolean isEditTaskRequested() {
        return (this instanceof EditTaskRequested);
    }

    /**
     * isTaskDeleted
     *
     * @return boolean
     */
    public final boolean isTaskDeleted() {
        return (this instanceof TaskDeleted);
    }

    /**
     * isTaskMarkedComplete
     *
     * @return boolean
     */
    public final boolean isTaskMarkedComplete() {
        return (this instanceof TaskMarkedComplete);
    }

    /**
     * isTaskMarkedActive
     *
     * @return boolean
     */
    public final boolean isTaskMarkedActive() {
        return (this instanceof TaskMarkedActive);
    }

    /**
     * isTaskSaveFailed
     *
     * @return boolean
     */
    public final boolean isTaskSaveFailed() {
        return (this instanceof TaskSaveFailed);
    }

    /**
     * isTaskDeletionFailed
     *
     * @return boolean
     */
    public final boolean isTaskDeletionFailed() {
        return (this instanceof TaskDeletionFailed);
    }

    /**
     * asDeleteTaskRequested
     *
     * @return DeleteTaskRequested
     */
    public final DeleteTaskRequested asDeleteTaskRequested() {
        return (DeleteTaskRequested) this;
    }

    /**
     * asCompleteTaskRequested
     *
     * @return CompleteTaskRequested
     */
    public final CompleteTaskRequested asCompleteTaskRequested() {
        return (CompleteTaskRequested) this;
    }

    /**
     * asActivateTaskRequested
     *
     * @return ActivateTaskRequested
     */
    public final ActivateTaskRequested asActivateTaskRequested() {
        return (ActivateTaskRequested) this;
    }

    /**
     * asEditTaskRequested
     *
     * @return EditTaskRequested
     */
    public final EditTaskRequested asEditTaskRequested() {
        return (EditTaskRequested) this;
    }

    /**
     * asTaskDeleted
     *
     * @return TaskDeleted
     */
    public final TaskDeleted asTaskDeleted() {
        return (TaskDeleted) this;
    }

    /**
     * asTaskMarkedComplete
     *
     * @return TaskMarkedComplete
     */
    public final TaskMarkedComplete asTaskMarkedComplete() {
        return (TaskMarkedComplete) this;
    }

    /**
     * asTaskMarkedActive
     *
     * @return TaskMarkedActive
     */
    public final TaskMarkedActive asTaskMarkedActive() {
        return (TaskMarkedActive) this;
    }

    /**
     * asTaskSaveFailed
     *
     * @return TaskSaveFailed
     */
    public final TaskSaveFailed asTaskSaveFailed() {
        return (TaskSaveFailed) this;
    }

    /**
     * asTaskDeletionFailed
     *
     * @return TaskDeletionFailed
     */
    public final TaskDeletionFailed asTaskDeletionFailed() {
        return (TaskDeletionFailed) this;
    }

    /**
     * match
     *
     * @param deleteTaskRequested deleteTaskRequested
     * @param completeTaskRequested completeTaskRequested
     * @param activateTaskRequested activateTaskRequested
     * @param editTaskRequested editTaskRequested
     * @param taskDeleted taskDeleted
     * @param taskMarkedComplete taskMarkedComplete
     * @param taskMarkedActive taskMarkedActive
     * @param taskSaveFailed taskSaveFailed
     * @param taskDeletionFailed taskDeletionFailed
     */
    public abstract void match(@Nonnull Consumer<DeleteTaskRequested> deleteTaskRequested,
        @Nonnull Consumer<CompleteTaskRequested> completeTaskRequested,
        @Nonnull Consumer<ActivateTaskRequested> activateTaskRequested,
        @Nonnull Consumer<EditTaskRequested> editTaskRequested,
        @Nonnull Consumer<TaskDeleted> taskDeleted,
        @Nonnull Consumer<TaskMarkedComplete> taskMarkedComplete,
        @Nonnull Consumer<TaskMarkedActive> taskMarkedActive,
        @Nonnull Consumer<TaskSaveFailed> taskSaveFailed,
        @Nonnull Consumer<TaskDeletionFailed> taskDeletionFailed);

    /**
     * map
     *
     * @param deleteTaskRequested deleteTaskRequested
     * @param completeTaskRequested completeTaskRequested
     * @param activateTaskRequested activateTaskRequested
     * @param editTaskRequested editTaskRequested
     * @param taskDeleted taskDeleted
     * @param taskMarkedComplete taskMarkedComplete
     * @param taskMarkedActive taskMarkedActive
     * @param taskSaveFailed taskSaveFailed
     * @param taskDeletionFailed taskDeletionFailed
     * @param <R_> R_
     * @return R_
     */
    public abstract <R_> R_ map(@Nonnull Function<DeleteTaskRequested, R_> deleteTaskRequested,
        @Nonnull Function<CompleteTaskRequested, R_> completeTaskRequested,
        @Nonnull Function<ActivateTaskRequested, R_> activateTaskRequested,
        @Nonnull Function<EditTaskRequested, R_> editTaskRequested,
        @Nonnull Function<TaskDeleted, R_> taskDeleted,
        @Nonnull Function<TaskMarkedComplete, R_> taskMarkedComplete,
        @Nonnull Function<TaskMarkedActive, R_> taskMarkedActive,
        @Nonnull Function<TaskSaveFailed, R_> taskSaveFailed,
        @Nonnull Function<TaskDeletionFailed, R_> taskDeletionFailed);

    /**
     * DeleteTaskRequested
     */
    public static final class DeleteTaskRequested extends TaskDetailEvent {
        DeleteTaskRequested() {
        }

        @Override
        public boolean equals(Object other) {
            return other instanceof DeleteTaskRequested;
        }

        @Override
        public int hashCode() {
            return 0;
        }

        @Override
        public String toString() {
            return "DeleteTaskRequested{}";
        }

        @Override
        public final void match(@Nonnull Consumer<DeleteTaskRequested> deleteTaskRequested,
            @Nonnull Consumer<CompleteTaskRequested> completeTaskRequested,
            @Nonnull Consumer<ActivateTaskRequested> activateTaskRequested,
            @Nonnull Consumer<EditTaskRequested> editTaskRequested,
            @Nonnull Consumer<TaskDeleted> taskDeleted,
            @Nonnull Consumer<TaskMarkedComplete> taskMarkedComplete,
            @Nonnull Consumer<TaskMarkedActive> taskMarkedActive,
            @Nonnull Consumer<TaskSaveFailed> taskSaveFailed,
            @Nonnull Consumer<TaskDeletionFailed> taskDeletionFailed) {
            deleteTaskRequested.accept(this);
        }

        @Override
        public final <R_> R_ map(@Nonnull Function<DeleteTaskRequested, R_> deleteTaskRequested,
            @Nonnull Function<CompleteTaskRequested, R_> completeTaskRequested,
            @Nonnull Function<ActivateTaskRequested, R_> activateTaskRequested,
            @Nonnull Function<EditTaskRequested, R_> editTaskRequested,
            @Nonnull Function<TaskDeleted, R_> taskDeleted,
            @Nonnull Function<TaskMarkedComplete, R_> taskMarkedComplete,
            @Nonnull Function<TaskMarkedActive, R_> taskMarkedActive,
            @Nonnull Function<TaskSaveFailed, R_> taskSaveFailed,
            @Nonnull Function<TaskDeletionFailed, R_> taskDeletionFailed) {
            return deleteTaskRequested.apply(this);
        }
    }

    /**
     * CompleteTaskRequested
     */
    public static final class CompleteTaskRequested extends TaskDetailEvent {
        CompleteTaskRequested() {
        }

        @Override
        public boolean equals(Object other) {
            return other instanceof CompleteTaskRequested;
        }

        @Override
        public int hashCode() {
            return 0;
        }

        @Override
        public String toString() {
            return "CompleteTaskRequested{}";
        }

        @Override
        public final void match(@Nonnull Consumer<DeleteTaskRequested> deleteTaskRequested,
            @Nonnull Consumer<CompleteTaskRequested> completeTaskRequested,
            @Nonnull Consumer<ActivateTaskRequested> activateTaskRequested,
            @Nonnull Consumer<EditTaskRequested> editTaskRequested,
            @Nonnull Consumer<TaskDeleted> taskDeleted,
            @Nonnull Consumer<TaskMarkedComplete> taskMarkedComplete,
            @Nonnull Consumer<TaskMarkedActive> taskMarkedActive,
            @Nonnull Consumer<TaskSaveFailed> taskSaveFailed,
            @Nonnull Consumer<TaskDeletionFailed> taskDeletionFailed) {
            completeTaskRequested.accept(this);
        }

        @Override
        public final <R_> R_ map(@Nonnull Function<DeleteTaskRequested, R_> deleteTaskRequested,
            @Nonnull Function<CompleteTaskRequested, R_> completeTaskRequested,
            @Nonnull Function<ActivateTaskRequested, R_> activateTaskRequested,
            @Nonnull Function<EditTaskRequested, R_> editTaskRequested,
            @Nonnull Function<TaskDeleted, R_> taskDeleted,
            @Nonnull Function<TaskMarkedComplete, R_> taskMarkedComplete,
            @Nonnull Function<TaskMarkedActive, R_> taskMarkedActive,
            @Nonnull Function<TaskSaveFailed, R_> taskSaveFailed,
            @Nonnull Function<TaskDeletionFailed, R_> taskDeletionFailed) {
            return completeTaskRequested.apply(this);
        }
    }

    /**
     * ActivateTaskRequested
     */
    public static final class ActivateTaskRequested extends TaskDetailEvent {
        ActivateTaskRequested() {
        }

        @Override
        public boolean equals(Object other) {
            return other instanceof ActivateTaskRequested;
        }

        @Override
        public int hashCode() {
            return 0;
        }

        @Override
        public String toString() {
            return "ActivateTaskRequested{}";
        }

        @Override
        public final void match(@Nonnull Consumer<DeleteTaskRequested> deleteTaskRequested,
            @Nonnull Consumer<CompleteTaskRequested> completeTaskRequested,
            @Nonnull Consumer<ActivateTaskRequested> activateTaskRequested,
            @Nonnull Consumer<EditTaskRequested> editTaskRequested,
            @Nonnull Consumer<TaskDeleted> taskDeleted,
            @Nonnull Consumer<TaskMarkedComplete> taskMarkedComplete,
            @Nonnull Consumer<TaskMarkedActive> taskMarkedActive,
            @Nonnull Consumer<TaskSaveFailed> taskSaveFailed,
            @Nonnull Consumer<TaskDeletionFailed> taskDeletionFailed) {
            activateTaskRequested.accept(this);
        }

        @Override
        public final <R_> R_ map(@Nonnull Function<DeleteTaskRequested, R_> deleteTaskRequested,
            @Nonnull Function<CompleteTaskRequested, R_> completeTaskRequested,
            @Nonnull Function<ActivateTaskRequested, R_> activateTaskRequested,
            @Nonnull Function<EditTaskRequested, R_> editTaskRequested,
            @Nonnull Function<TaskDeleted, R_> taskDeleted,
            @Nonnull Function<TaskMarkedComplete, R_> taskMarkedComplete,
            @Nonnull Function<TaskMarkedActive, R_> taskMarkedActive,
            @Nonnull Function<TaskSaveFailed, R_> taskSaveFailed,
            @Nonnull Function<TaskDeletionFailed, R_> taskDeletionFailed) {
            return activateTaskRequested.apply(this);
        }
    }

    /**
     * EditTaskRequested
     */
    public static final class EditTaskRequested extends TaskDetailEvent {
        EditTaskRequested() {
        }

        @Override
        public boolean equals(Object other) {
            return other instanceof EditTaskRequested;
        }

        @Override
        public int hashCode() {
            return 0;
        }

        @Override
        public String toString() {
            return "EditTaskRequested{}";
        }

        @Override
        public final void match(@Nonnull Consumer<DeleteTaskRequested> deleteTaskRequested,
            @Nonnull Consumer<CompleteTaskRequested> completeTaskRequested,
            @Nonnull Consumer<ActivateTaskRequested> activateTaskRequested,
            @Nonnull Consumer<EditTaskRequested> editTaskRequested,
            @Nonnull Consumer<TaskDeleted> taskDeleted,
            @Nonnull Consumer<TaskMarkedComplete> taskMarkedComplete,
            @Nonnull Consumer<TaskMarkedActive> taskMarkedActive,
            @Nonnull Consumer<TaskSaveFailed> taskSaveFailed,
            @Nonnull Consumer<TaskDeletionFailed> taskDeletionFailed) {
            editTaskRequested.accept(this);
        }

        @Override
        public final <R_> R_ map(@Nonnull Function<DeleteTaskRequested, R_> deleteTaskRequested,
            @Nonnull Function<CompleteTaskRequested, R_> completeTaskRequested,
            @Nonnull Function<ActivateTaskRequested, R_> activateTaskRequested,
            @Nonnull Function<EditTaskRequested, R_> editTaskRequested,
            @Nonnull Function<TaskDeleted, R_> taskDeleted,
            @Nonnull Function<TaskMarkedComplete, R_> taskMarkedComplete,
            @Nonnull Function<TaskMarkedActive, R_> taskMarkedActive,
            @Nonnull Function<TaskSaveFailed, R_> taskSaveFailed,
            @Nonnull Function<TaskDeletionFailed, R_> taskDeletionFailed) {
            return editTaskRequested.apply(this);
        }
    }

    /**
     * TaskDeleted
     */
    public static final class TaskDeleted extends TaskDetailEvent {
        TaskDeleted() {
        }

        @Override
        public boolean equals(Object other) {
            return other instanceof TaskDeleted;
        }

        @Override
        public int hashCode() {
            return 0;
        }

        @Override
        public String toString() {
            return "TaskDeleted{}";
        }

        @Override
        public final void match(@Nonnull Consumer<DeleteTaskRequested> deleteTaskRequested,
            @Nonnull Consumer<CompleteTaskRequested> completeTaskRequested,
            @Nonnull Consumer<ActivateTaskRequested> activateTaskRequested,
            @Nonnull Consumer<EditTaskRequested> editTaskRequested,
            @Nonnull Consumer<TaskDeleted> taskDeleted,
            @Nonnull Consumer<TaskMarkedComplete> taskMarkedComplete,
            @Nonnull Consumer<TaskMarkedActive> taskMarkedActive,
            @Nonnull Consumer<TaskSaveFailed> taskSaveFailed,
            @Nonnull Consumer<TaskDeletionFailed> taskDeletionFailed) {
            taskDeleted.accept(this);
        }

        @Override
        public final <R_> R_ map(@Nonnull Function<DeleteTaskRequested, R_> deleteTaskRequested,
            @Nonnull Function<CompleteTaskRequested, R_> completeTaskRequested,
            @Nonnull Function<ActivateTaskRequested, R_> activateTaskRequested,
            @Nonnull Function<EditTaskRequested, R_> editTaskRequested,
            @Nonnull Function<TaskDeleted, R_> taskDeleted,
            @Nonnull Function<TaskMarkedComplete, R_> taskMarkedComplete,
            @Nonnull Function<TaskMarkedActive, R_> taskMarkedActive,
            @Nonnull Function<TaskSaveFailed, R_> taskSaveFailed,
            @Nonnull Function<TaskDeletionFailed, R_> taskDeletionFailed) {
            return taskDeleted.apply(this);
        }
    }

    /**
     * TaskMarkedComplete
     */
    public static final class TaskMarkedComplete extends TaskDetailEvent {
        TaskMarkedComplete() {
        }

        @Override
        public boolean equals(Object other) {
            return other instanceof TaskMarkedComplete;
        }

        @Override
        public int hashCode() {
            return 0;
        }

        @Override
        public String toString() {
            return "TaskMarkedComplete{}";
        }

        @Override
        public final void match(@Nonnull Consumer<DeleteTaskRequested> deleteTaskRequested,
            @Nonnull Consumer<CompleteTaskRequested> completeTaskRequested,
            @Nonnull Consumer<ActivateTaskRequested> activateTaskRequested,
            @Nonnull Consumer<EditTaskRequested> editTaskRequested,
            @Nonnull Consumer<TaskDeleted> taskDeleted,
            @Nonnull Consumer<TaskMarkedComplete> taskMarkedComplete,
            @Nonnull Consumer<TaskMarkedActive> taskMarkedActive,
            @Nonnull Consumer<TaskSaveFailed> taskSaveFailed,
            @Nonnull Consumer<TaskDeletionFailed> taskDeletionFailed) {
            taskMarkedComplete.accept(this);
        }

        @Override
        public final <R_> R_ map(@Nonnull Function<DeleteTaskRequested, R_> deleteTaskRequested,
            @Nonnull Function<CompleteTaskRequested, R_> completeTaskRequested,
            @Nonnull Function<ActivateTaskRequested, R_> activateTaskRequested,
            @Nonnull Function<EditTaskRequested, R_> editTaskRequested,
            @Nonnull Function<TaskDeleted, R_> taskDeleted,
            @Nonnull Function<TaskMarkedComplete, R_> taskMarkedComplete,
            @Nonnull Function<TaskMarkedActive, R_> taskMarkedActive,
            @Nonnull Function<TaskSaveFailed, R_> taskSaveFailed,
            @Nonnull Function<TaskDeletionFailed, R_> taskDeletionFailed) {
            return taskMarkedComplete.apply(this);
        }
    }

    /**
     * TaskMarkedActive
     */
    public static final class TaskMarkedActive extends TaskDetailEvent {
        TaskMarkedActive() {
        }

        @Override
        public boolean equals(Object other) {
            return other instanceof TaskMarkedActive;
        }

        @Override
        public int hashCode() {
            return 0;
        }

        @Override
        public String toString() {
            return "TaskMarkedActive{}";
        }

        @Override
        public final void match(@Nonnull Consumer<DeleteTaskRequested> deleteTaskRequested,
            @Nonnull Consumer<CompleteTaskRequested> completeTaskRequested,
            @Nonnull Consumer<ActivateTaskRequested> activateTaskRequested,
            @Nonnull Consumer<EditTaskRequested> editTaskRequested,
            @Nonnull Consumer<TaskDeleted> taskDeleted,
            @Nonnull Consumer<TaskMarkedComplete> taskMarkedComplete,
            @Nonnull Consumer<TaskMarkedActive> taskMarkedActive,
            @Nonnull Consumer<TaskSaveFailed> taskSaveFailed,
            @Nonnull Consumer<TaskDeletionFailed> taskDeletionFailed) {
            taskMarkedActive.accept(this);
        }

        @Override
        public final <R_> R_ map(@Nonnull Function<DeleteTaskRequested, R_> deleteTaskRequested,
            @Nonnull Function<CompleteTaskRequested, R_> completeTaskRequested,
            @Nonnull Function<ActivateTaskRequested, R_> activateTaskRequested,
            @Nonnull Function<EditTaskRequested, R_> editTaskRequested,
            @Nonnull Function<TaskDeleted, R_> taskDeleted,
            @Nonnull Function<TaskMarkedComplete, R_> taskMarkedComplete,
            @Nonnull Function<TaskMarkedActive, R_> taskMarkedActive,
            @Nonnull Function<TaskSaveFailed, R_> taskSaveFailed,
            @Nonnull Function<TaskDeletionFailed, R_> taskDeletionFailed) {
            return taskMarkedActive.apply(this);
        }
    }

    /**
     * TaskSaveFailed
     */
    public static final class TaskSaveFailed extends TaskDetailEvent {
        TaskSaveFailed() {
        }

        @Override
        public boolean equals(Object other) {
            return other instanceof TaskSaveFailed;
        }

        @Override
        public int hashCode() {
            return 0;
        }

        @Override
        public String toString() {
            return "TaskSaveFailed{}";
        }

        @Override
        public final void match(@Nonnull Consumer<DeleteTaskRequested> deleteTaskRequested,
            @Nonnull Consumer<CompleteTaskRequested> completeTaskRequested,
            @Nonnull Consumer<ActivateTaskRequested> activateTaskRequested,
            @Nonnull Consumer<EditTaskRequested> editTaskRequested,
            @Nonnull Consumer<TaskDeleted> taskDeleted,
            @Nonnull Consumer<TaskMarkedComplete> taskMarkedComplete,
            @Nonnull Consumer<TaskMarkedActive> taskMarkedActive,
            @Nonnull Consumer<TaskSaveFailed> taskSaveFailed,
            @Nonnull Consumer<TaskDeletionFailed> taskDeletionFailed) {
            taskSaveFailed.accept(this);
        }

        @Override
        public final <R_> R_ map(@Nonnull Function<DeleteTaskRequested, R_> deleteTaskRequested,
            @Nonnull Function<CompleteTaskRequested, R_> completeTaskRequested,
            @Nonnull Function<ActivateTaskRequested, R_> activateTaskRequested,
            @Nonnull Function<EditTaskRequested, R_> editTaskRequested,
            @Nonnull Function<TaskDeleted, R_> taskDeleted,
            @Nonnull Function<TaskMarkedComplete, R_> taskMarkedComplete,
            @Nonnull Function<TaskMarkedActive, R_> taskMarkedActive,
            @Nonnull Function<TaskSaveFailed, R_> taskSaveFailed,
            @Nonnull Function<TaskDeletionFailed, R_> taskDeletionFailed) {
            return taskSaveFailed.apply(this);
        }
    }

    public static final class TaskDeletionFailed extends TaskDetailEvent {
        TaskDeletionFailed() {
        }

        @Override
        public boolean equals(Object other) {
            return other instanceof TaskDeletionFailed;
        }

        /**
         * hashCode
         *
         * @return int
         */
        @Override
        public int hashCode() {
            return 0;
        }

        @Override
        public String toString() {
            return "TaskDeletionFailed{}";
        }

        @Override
        public final void match(@Nonnull Consumer<DeleteTaskRequested> deleteTaskRequested,
            @Nonnull Consumer<CompleteTaskRequested> completeTaskRequested,
            @Nonnull Consumer<ActivateTaskRequested> activateTaskRequested,
            @Nonnull Consumer<EditTaskRequested> editTaskRequested,
            @Nonnull Consumer<TaskDeleted> taskDeleted,
            @Nonnull Consumer<TaskMarkedComplete> taskMarkedComplete,
            @Nonnull Consumer<TaskMarkedActive> taskMarkedActive,
            @Nonnull Consumer<TaskSaveFailed> taskSaveFailed,
            @Nonnull Consumer<TaskDeletionFailed> taskDeletionFailed) {
            taskDeletionFailed.accept(this);
        }

        @Override
        public final <R_> R_ map(@Nonnull Function<DeleteTaskRequested, R_> deleteTaskRequested,
            @Nonnull Function<CompleteTaskRequested, R_> completeTaskRequested,
            @Nonnull Function<ActivateTaskRequested, R_> activateTaskRequested,
            @Nonnull Function<EditTaskRequested, R_> editTaskRequested,
            @Nonnull Function<TaskDeleted, R_> taskDeleted,
            @Nonnull Function<TaskMarkedComplete, R_> taskMarkedComplete,
            @Nonnull Function<TaskMarkedActive, R_> taskMarkedActive,
            @Nonnull Function<TaskSaveFailed, R_> taskSaveFailed,
            @Nonnull Function<TaskDeletionFailed, R_> taskDeletionFailed) {
            return taskDeletionFailed.apply(this);
        }
    }
}
