/*
 * -\-\-
 * --
 * Copyright (c) 2017-2018 Spotify AB
 * --
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * -/-/-
 */

package com.example.ohos.architecture.blueprints.todoapp.taskdetail.view;

/**
 * TaskDetailViewData
 */
public class TaskDetailViewData {
    private TextViewData title;
    private TextViewData description;
    private boolean completedChecked;

    private TaskDetailViewData(TextViewData title, TextViewData description, boolean completedChecked) {
        this.title = title;
        this.description = description;
        this.completedChecked = completedChecked;
    }

    /**
     * title
     *
     * @return TextViewData
     */
    public TextViewData title() {
        return title;
    }

    /**
     * description
     *
     * @return TextViewData
     */
    public TextViewData description() {
        return description;
    }

    /**
     * completedChecked
     *
     * @return boolean
     */
    public boolean completedChecked() {
        return completedChecked;
    }

    @Override
    public String toString() {
        return "TaskDetailViewData{"
            + "title=" + title + ", "
            + "description=" + description + ", "
            + "completedChecked=" + completedChecked
            + "}";
    }

    /**
     * builder
     *
     * @return Builder
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * Builder
     */
    public static class Builder {
        private TaskDetailViewData.TextViewData title;
        private TaskDetailViewData.TextViewData description;
        private Boolean completedChecked;

        Builder() {
        }

        /**
         * title
         *
         * @param title title
         * @return TaskDetailViewData.Builder
         */
        public TaskDetailViewData.Builder title(TaskDetailViewData.TextViewData title) {
            if (title == null) {
                throw new NullPointerException("Null title");
            }
            this.title = title;
            return this;
        }

        /**
         * description
         *
         * @param description description
         * @return TaskDetailViewData.Builder
         */
        public TaskDetailViewData.Builder description(TaskDetailViewData.TextViewData description) {
            if (description == null) {
                throw new NullPointerException("Null description");
            }
            this.description = description;
            return this;
        }

        /**
         * completedChecked
         *
         * @param completedChecked completedChecked
         * @return TaskDetailViewData.Builder
         */
        public TaskDetailViewData.Builder completedChecked(boolean completedChecked) {
            this.completedChecked = completedChecked;
            return this;
        }

        /**
         * build
         *
         * @return TaskDetailViewData
         */
        public TaskDetailViewData build() {
            String missing = "";
            if (this.title == null) {
                missing += " title";
            }
            if (this.description == null) {
                missing += " description";
            }
            if (this.completedChecked == null) {
                missing += " completedChecked";
            }
            if (!missing.isEmpty()) {
                throw new IllegalStateException("Missing required properties:" + missing);
            }
            return new TaskDetailViewData(
                this.title,
                this.description,
                this.completedChecked);
        }
    }

    /**
     * TextViewData
     */
    public static class TextViewData {
        private int visibility;
        private String text;

        TextViewData(
            int visibility,
            String text) {
            this.visibility = visibility;
            if (text == null) {
                throw new NullPointerException("Null text");
            }
            this.text = text;
        }

        /**
         * visibility
         *
         * @return int
         */
        public int visibility() {
            return visibility;
        }

        /**
         * text
         *
         * @return String
         */
        public String text() {
            return text;
        }

        @Override
        public String toString() {
            return "TextViewData{"
                + "visibility=" + visibility + ", "
                + "text=" + text
                + "}";
        }

        /**
         * create
         *
         * @param visibility visibility
         * @param text text
         * @return TextViewData
         */
        public static TextViewData create(int visibility, String text) {
            return new TextViewData(visibility, text);
        }
    }
}
