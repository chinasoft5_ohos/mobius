/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.ohos.architecture.blueprints.todoapp.util;

import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;

import java.util.HashSet;
import java.util.Set;

/**
 * @since 2021-07-12
 */
public class PreferencesUtil {
    private static Context instance;

    private static final String PREF_NAME = "MOBIUS_str";

    /**
     * init
     *
     * @param context context
     */
    public static void init(Context context) {
        instance = context;
    }

    /**
     * saveString
     *
     * @param key key
     * @param value value
     */
    public static void saveString(String key, String value) {
        Preferences sharedPrefs = new DatabaseHelper(instance).getPreferences(PREF_NAME);
        sharedPrefs.putString(key, value).flush();
    }

    /**
     * getString
     *
     * @param key key
     * @return String
     */
    public static String getString(String key) {
        Preferences sharedPrefs = new DatabaseHelper(instance).getPreferences(PREF_NAME);
        return sharedPrefs.getString(key, "");
    }

    /**
     * saveStringArray
     *
     * @param key key
     * @param value value
     */
    public static void saveStringArray(String key, Set<String> value) {
        LogUtil.debug("MOBIUSTag", "instance#" + instance);
        Preferences sharedPrefs = new DatabaseHelper(instance).getPreferences(PREF_NAME);
        LogUtil.debug("MOBIUSTag", "sharedPrefs#" + sharedPrefs);
        sharedPrefs.putStringSet(key, value).flush();
    }

    /**
     * getStringArray
     *
     * @param key key
     * @return Set<String>
     */
    public static Set<String> getStringArray(String key) {
        Preferences sharedPrefs = new DatabaseHelper(instance).getPreferences(PREF_NAME);
        return sharedPrefs.getStringSet(key, new HashSet<>());
    }
}
