/*
 * Copyright 2016, The Android Open Source Project
 * Copyright (c) 2017-2018 Spotify AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.ohos.architecture.blueprints.todoapp.addedittask.view;

import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.Button;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;

import com.example.ohos.architecture.blueprints.todoapp.addedittask.domain.AddEditTaskEvent;
import com.example.ohos.architecture.blueprints.todoapp.addedittask.domain.AddEditTaskModel;
import com.example.ohos.architecture.blueprints.todoapp.data.Task;
import com.ohos.mobius.ResourceTable;
import com.spotify.mobius.Connectable;
import com.spotify.mobius.Connection;
import com.spotify.mobius.functions.Consumer;

import javax.annotation.Nonnull;

import static com.example.ohos.architecture.blueprints.todoapp.addedittask.domain.AddEditTaskEvent.taskDefinitionCompleted;

/**
 * @since 2021-06-08
 */
public class AddEditTaskViews implements Connectable<AddEditTaskModel, AddEditTaskEvent> {
    private Button mFab;
    private Text mTitle;
    private Text mDescription;
    private Context context;

    /**
     * AddEditTaskViews
     *
     * @param abilitySlice abilitySlice
     * @param task task
     */
    public AddEditTaskViews(AbilitySlice abilitySlice, Task task) {
        context = abilitySlice.getContext();
        mTitle = (Text) abilitySlice.findComponentById(ResourceTable.Id_text_title);
        mDescription = (Text) abilitySlice.findComponentById(ResourceTable.Id_text_dic);
        mFab = (Button) abilitySlice.findComponentById(ResourceTable.Id_bt_add);
        mFab.setText(task == null ? "Add" : "Edit");
        mTitle.addTextObserver(new Text.TextObserver() {
            @Override
            public void onTextUpdated(String str, int i0, int i1, int i2) {
                if (str.length() > 80) {
                    mTitle.setText(str.substring(0, 80));
                    showToast("TO do title The length cannot exceed 80");
                }
            }
        });
        mDescription.addTextObserver(new Text.TextObserver() {
            @Override
            public void onTextUpdated(String str, int i0, int i1, int i2) {
                if (str.length() > 200) {
                    mDescription.setText(str.substring(0, 200));
                    showToast("TO do title The length cannot exceed 200");
                }
            }
        });
    }

    /**
     * showEmptyTaskError
     */
    public void showEmptyTaskError() {
        showToast("TO DOs cannot be empty");
    }

    private void showToast(String msg) {
        DirectionalLayout toastLayout = (DirectionalLayout) LayoutScatter.getInstance(context)
            .parse(ResourceTable.Layout_layout_toast, null, false);
        Text text = (Text) toastLayout.findComponentById(ResourceTable.Id_msg_toast);
        text.setText(msg);
        ToastDialog toast = new ToastDialog(context);
        toast.setContentCustomComponent(toastLayout);
        toast.setSize(DirectionalLayout.LayoutConfig.MATCH_CONTENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        toast.setAlignment(LayoutAlignment.BOTTOM);
        toast.show();
    }

    /**
     * setTitle
     *
     * @param title title
     */
    public void setTitle(String title) {
        mTitle.setText(title);
    }

    /**
     * setDescription
     *
     * @param description description
     */
    public void setDescription(String description) {
        mDescription.setText(description);
    }

    @Nonnull
    @Override
    public Connection<AddEditTaskModel> connect(Consumer<AddEditTaskEvent> output) {
        mFab.setClickedListener(
            __ -> {
                if (mTitle.getText().isEmpty()) {
                    showToast("TO do title cannot be empty");
                    return;
                }
                output.accept(taskDefinitionCompleted(mTitle.getText(), mDescription.getText()));

            });
        return new Connection<AddEditTaskModel>() {
            @Override
            public void accept(AddEditTaskModel model) {
                setTitle(model.details().title());
                setDescription(model.details().description());
            }

            @Override
            public void dispose() {
                mFab.setClickedListener(null);
            }
        };
    }
}
