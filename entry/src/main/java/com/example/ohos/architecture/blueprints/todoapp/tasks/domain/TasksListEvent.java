package com.example.ohos.architecture.blueprints.todoapp.tasks.domain;

import static com.spotify.dataenum.DataenumUtils.checkNotNull;

import com.example.ohos.architecture.blueprints.todoapp.data.Task;
import com.google.common.collect.ImmutableList;
import com.spotify.dataenum.function.Consumer;
import com.spotify.dataenum.function.Function;

import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.StringBuilder;

import javax.annotation.Generated;
import javax.annotation.Nonnull;

/**
 * TasksListEvent
 */
@Generated("com.spotify.dataenum.processor.DataEnumProcessor")
public abstract class TasksListEvent {
    TasksListEvent() {
    }

    /**
     * refreshRequested
     *
     * @return TasksListEvent
     */
    public static TasksListEvent refreshRequested() {
        return new RefreshRequested();
    }

    /**
     * newTaskClicked
     *
     * @return TasksListEvent
     */
    public static TasksListEvent newTaskClicked() {
        return new NewTaskClicked();
    }

    /**
     * navigateToTaskDetailsRequested
     *
     * @param taskId taskId
     * @return TasksListEvent
     */
    public static TasksListEvent navigateToTaskDetailsRequested(@Nonnull String taskId) {
        return new NavigateToTaskDetailsRequested(taskId);
    }

    /**
     * taskMarkedComplete
     *
     * @param taskId taskId
     * @return TasksListEvent
     */
    public static TasksListEvent taskMarkedComplete(@Nonnull String taskId) {
        return new TaskMarkedComplete(taskId);
    }

    /**
     * taskMarkedActive
     *
     * @param taskId taskId
     * @return TasksListEvent
     */
    public static TasksListEvent taskMarkedActive(@Nonnull String taskId) {
        return new TaskMarkedActive(taskId);
    }

    /**
     * TasksListEvent
     *
     * @return clearCompletedTasksRequested
     */
    public static TasksListEvent clearCompletedTasksRequested() {
        return new ClearCompletedTasksRequested();
    }

    /**
     * filterSelected
     *
     * @param filterType filterType
     * @return TasksListEvent
     */
    public static TasksListEvent filterSelected(@Nonnull TasksFilterType filterType) {
        return new FilterSelected(filterType);
    }

    /**
     * tasksLoaded
     *
     * @param tasks tasks
     * @return TasksListEvent
     */
    public static TasksListEvent tasksLoaded(@Nonnull ImmutableList<Task> tasks) {
        return new TasksLoaded(tasks);
    }

    /**
     * taskCreated
     *
     * @return TasksListEvent
     */
    public static TasksListEvent taskCreated() {
        return new TaskCreated();
    }

    /**
     * tasksRefreshed
     *
     * @return TasksListEvent
     */
    public static TasksListEvent tasksRefreshed() {
        return new TasksRefreshed();
    }

    /**
     * tasksRefreshFailed
     *
     * @return TasksListEvent
     */
    public static TasksListEvent tasksRefreshFailed() {
        return new TasksRefreshFailed();
    }

    /**
     * tasksLoadingFailed
     *
     * @return TasksListEvent
     */
    public static TasksListEvent tasksLoadingFailed() {
        return new TasksLoadingFailed();
    }

    /**
     * isRefreshRequested
     *
     * @return boolean
     */
    public final boolean isRefreshRequested() {
        return (this instanceof RefreshRequested);
    }

    /**
     * isNewTaskClicked
     *
     * @return boolean
     */
    public final boolean isNewTaskClicked() {
        return (this instanceof NewTaskClicked);
    }

    /**
     * isNavigateToTaskDetailsRequested
     *
     * @return boolean
     */
    public final boolean isNavigateToTaskDetailsRequested() {
        return (this instanceof NavigateToTaskDetailsRequested);
    }

    /**
     * isTaskMarkedComplete
     *
     * @return boolean
     */
    public final boolean isTaskMarkedComplete() {
        return (this instanceof TaskMarkedComplete);
    }

    /**
     * isTaskMarkedActive
     *
     * @return boolean
     */
    public final boolean isTaskMarkedActive() {
        return (this instanceof TaskMarkedActive);
    }

    /**
     * isClearCompletedTasksRequested
     *
     * @return boolean
     */
    public final boolean isClearCompletedTasksRequested() {
        return (this instanceof ClearCompletedTasksRequested);
    }

    /**
     * isFilterSelected
     *
     * @return boolean
     */
    public final boolean isFilterSelected() {
        return (this instanceof FilterSelected);
    }

    /**
     * isTasksLoaded
     *
     * @return boolean
     */
    public final boolean isTasksLoaded() {
        return (this instanceof TasksLoaded);
    }

    /**
     * isTaskCreated
     *
     * @return boolean
     */
    public final boolean isTaskCreated() {
        return (this instanceof TaskCreated);
    }

    /**
     * isTasksRefreshed
     *
     * @return boolean
     */
    public final boolean isTasksRefreshed() {
        return (this instanceof TasksRefreshed);
    }

    /**
     * isTasksRefreshFailed
     *
     * @return boolean
     */
    public final boolean isTasksRefreshFailed() {
        return (this instanceof TasksRefreshFailed);
    }

    /**
     * isTasksLoadingFailed
     *
     * @return boolean
     */
    public final boolean isTasksLoadingFailed() {
        return (this instanceof TasksLoadingFailed);
    }

    /**
     * asRefreshRequested
     *
     * @return RefreshRequested
     */
    public final RefreshRequested asRefreshRequested() {
        return (RefreshRequested) this;
    }

    /**
     * asNewTaskClicked
     *
     * @return NewTaskClicked
     */
    public final NewTaskClicked asNewTaskClicked() {
        return (NewTaskClicked) this;
    }

    /**
     * asNavigateToTaskDetailsRequested
     *
     * @return NavigateToTaskDetailsRequested
     */
    public final NavigateToTaskDetailsRequested asNavigateToTaskDetailsRequested() {
        return (NavigateToTaskDetailsRequested) this;
    }

    /**
     * asTaskMarkedComplete
     *
     * @return TaskMarkedComplete
     */
    public final TaskMarkedComplete asTaskMarkedComplete() {
        return (TaskMarkedComplete) this;
    }

    /**
     * asTaskMarkedActive
     *
     * @return TaskMarkedActive
     */
    public final TaskMarkedActive asTaskMarkedActive() {
        return (TaskMarkedActive) this;
    }

    /**
     * asClearCompletedTasksRequested
     *
     * @return ClearCompletedTasksRequested
     */
    public final ClearCompletedTasksRequested asClearCompletedTasksRequested() {
        return (ClearCompletedTasksRequested) this;
    }

    /**
     * asFilterSelected
     *
     * @return FilterSelected
     */
    public final FilterSelected asFilterSelected() {
        return (FilterSelected) this;
    }

    /**
     * asTasksLoaded
     *
     * @return TasksLoaded
     */
    public final TasksLoaded asTasksLoaded() {
        return (TasksLoaded) this;
    }

    /**
     * asTaskCreated
     *
     * @return TaskCreated
     */
    public final TaskCreated asTaskCreated() {
        return (TaskCreated) this;
    }

    /**
     * asTasksRefreshed
     *
     * @return TasksRefreshed
     */
    public final TasksRefreshed asTasksRefreshed() {
        return (TasksRefreshed) this;
    }

    /**
     * asTasksRefreshFailed
     *
     * @return TasksRefreshFailed
     */
    public final TasksRefreshFailed asTasksRefreshFailed() {
        return (TasksRefreshFailed) this;
    }

    /**
     * asTasksLoadingFailed
     *
     * @return TasksLoadingFailed
     */
    public final TasksLoadingFailed asTasksLoadingFailed() {
        return (TasksLoadingFailed) this;
    }

    /**
     * match
     *
     * @param refreshRequested refreshRequested
     * @param newTaskClicked newTaskClicked
     * @param navigateToTaskDetailsRequested navigateToTaskDetailsRequested
     * @param taskMarkedComplete taskMarkedComplete
     * @param taskMarkedActive taskMarkedActive
     * @param clearCompletedTasksRequested clearCompletedTasksRequested
     * @param filterSelected filterSelected
     * @param tasksLoaded tasksLoaded
     * @param taskCreated taskCreated
     * @param tasksRefreshed tasksRefreshed
     * @param tasksRefreshFailed tasksRefreshFailed
     * @param tasksLoadingFailed tasksLoadingFailed
     */
    public abstract void match(@Nonnull Consumer<RefreshRequested> refreshRequested,
    @Nonnull Consumer<NewTaskClicked> newTaskClicked,
    @Nonnull Consumer<NavigateToTaskDetailsRequested> navigateToTaskDetailsRequested,
    @Nonnull Consumer<TaskMarkedComplete> taskMarkedComplete,
    @Nonnull Consumer<TaskMarkedActive> taskMarkedActive,
    @Nonnull Consumer<ClearCompletedTasksRequested> clearCompletedTasksRequested,
    @Nonnull Consumer<FilterSelected> filterSelected, @Nonnull Consumer<TasksLoaded> tasksLoaded,
    @Nonnull Consumer<TaskCreated> taskCreated, @Nonnull Consumer<TasksRefreshed> tasksRefreshed,
    @Nonnull Consumer<TasksRefreshFailed> tasksRefreshFailed,
    @Nonnull Consumer<TasksLoadingFailed> tasksLoadingFailed);

    /**
     * map
     *
     * @param refreshRequested refreshRequested
     * @param newTaskClicked newTaskClicked
     * @param navigateToTaskDetailsRequested navigateToTaskDetailsRequested
     * @param taskMarkedComplete taskMarkedComplete
     * @param taskMarkedActive taskMarkedActive
     * @param clearCompletedTasksRequested clearCompletedTasksRequested
     * @param filterSelected filterSelected
     * @param tasksLoaded tasksLoaded
     * @param taskCreated taskCreated
     * @param tasksRefreshed tasksRefreshed
     * @param tasksRefreshFailed tasksRefreshFailed
     * @param tasksLoadingFailed tasksLoadingFailed
     * @param <R_> R_
     * @return R_
     */
    public abstract <R_> R_ map(@Nonnull Function<RefreshRequested, R_> refreshRequested,
    @Nonnull Function<NewTaskClicked, R_> newTaskClicked,
    @Nonnull Function<NavigateToTaskDetailsRequested, R_> navigateToTaskDetailsRequested,
    @Nonnull Function<TaskMarkedComplete, R_> taskMarkedComplete,
    @Nonnull Function<TaskMarkedActive, R_> taskMarkedActive,
    @Nonnull Function<ClearCompletedTasksRequested, R_> clearCompletedTasksRequested,
    @Nonnull Function<FilterSelected, R_> filterSelected,
    @Nonnull Function<TasksLoaded, R_> tasksLoaded,
    @Nonnull Function<TaskCreated, R_> taskCreated,
    @Nonnull Function<TasksRefreshed, R_> tasksRefreshed,
    @Nonnull Function<TasksRefreshFailed, R_> tasksRefreshFailed,
    @Nonnull Function<TasksLoadingFailed, R_> tasksLoadingFailed);

    /**
     * RefreshRequested
     */
    public static final class RefreshRequested extends TasksListEvent {
        RefreshRequested() {
        }

        @Override
        public boolean equals(Object other) {
            return other instanceof RefreshRequested;
        }

        @Override
        public int hashCode() {
            return 0;
        }

        @Override
        public String toString() {
            return "RefreshRequested{}";
        }

        @Override
        public final void match(@Nonnull Consumer<RefreshRequested> refreshRequested,
            @Nonnull Consumer<NewTaskClicked> newTaskClicked,
            @Nonnull Consumer<NavigateToTaskDetailsRequested> navigateToTaskDetailsRequested,
            @Nonnull Consumer<TaskMarkedComplete> taskMarkedComplete,
            @Nonnull Consumer<TaskMarkedActive> taskMarkedActive,
            @Nonnull Consumer<ClearCompletedTasksRequested> clearCompletedTasksRequested,
            @Nonnull Consumer<FilterSelected> filterSelected,
            @Nonnull Consumer<TasksLoaded> tasksLoaded, @Nonnull Consumer<TaskCreated> taskCreated,
            @Nonnull Consumer<TasksRefreshed> tasksRefreshed,
            @Nonnull Consumer<TasksRefreshFailed> tasksRefreshFailed,
            @Nonnull Consumer<TasksLoadingFailed> tasksLoadingFailed) {
            refreshRequested.accept(this);
        }

        @Override
        public final <R_> R_ map(@Nonnull Function<RefreshRequested, R_> refreshRequested,
            @Nonnull Function<NewTaskClicked, R_> newTaskClicked,
            @Nonnull Function<NavigateToTaskDetailsRequested, R_> navigateToTaskDetailsRequested,
            @Nonnull Function<TaskMarkedComplete, R_> taskMarkedComplete,
            @Nonnull Function<TaskMarkedActive, R_> taskMarkedActive,
            @Nonnull Function<ClearCompletedTasksRequested, R_> clearCompletedTasksRequested,
            @Nonnull Function<FilterSelected, R_> filterSelected,
            @Nonnull Function<TasksLoaded, R_> tasksLoaded,
            @Nonnull Function<TaskCreated, R_> taskCreated,
            @Nonnull Function<TasksRefreshed, R_> tasksRefreshed,
            @Nonnull Function<TasksRefreshFailed, R_> tasksRefreshFailed,
            @Nonnull Function<TasksLoadingFailed, R_> tasksLoadingFailed) {
            return refreshRequested.apply(this);
        }
    }

    /**
     * NewTaskClicked
     */
    public static final class NewTaskClicked extends TasksListEvent {
        NewTaskClicked() {
        }

        @Override
        public boolean equals(Object other) {
            return other instanceof NewTaskClicked;
        }

        @Override
        public int hashCode() {
            return 0;
        }

        @Override
        public String toString() {
            return "NewTaskClicked{}";
        }

        @Override
        public final void match(@Nonnull Consumer<RefreshRequested> refreshRequested,
            @Nonnull Consumer<NewTaskClicked> newTaskClicked,
            @Nonnull Consumer<NavigateToTaskDetailsRequested> navigateToTaskDetailsRequested,
            @Nonnull Consumer<TaskMarkedComplete> taskMarkedComplete,
            @Nonnull Consumer<TaskMarkedActive> taskMarkedActive,
            @Nonnull Consumer<ClearCompletedTasksRequested> clearCompletedTasksRequested,
            @Nonnull Consumer<FilterSelected> filterSelected,
            @Nonnull Consumer<TasksLoaded> tasksLoaded, @Nonnull Consumer<TaskCreated> taskCreated,
            @Nonnull Consumer<TasksRefreshed> tasksRefreshed,
            @Nonnull Consumer<TasksRefreshFailed> tasksRefreshFailed,
            @Nonnull Consumer<TasksLoadingFailed> tasksLoadingFailed) {
            newTaskClicked.accept(this);
        }

        @Override
        public final <R_> R_ map(@Nonnull Function<RefreshRequested, R_> refreshRequested,
            @Nonnull Function<NewTaskClicked, R_> newTaskClicked,
            @Nonnull Function<NavigateToTaskDetailsRequested, R_> navigateToTaskDetailsRequested,
            @Nonnull Function<TaskMarkedComplete, R_> taskMarkedComplete,
            @Nonnull Function<TaskMarkedActive, R_> taskMarkedActive,
            @Nonnull Function<ClearCompletedTasksRequested, R_> clearCompletedTasksRequested,
            @Nonnull Function<FilterSelected, R_> filterSelected,
            @Nonnull Function<TasksLoaded, R_> tasksLoaded,
            @Nonnull Function<TaskCreated, R_> taskCreated,
            @Nonnull Function<TasksRefreshed, R_> tasksRefreshed,
            @Nonnull Function<TasksRefreshFailed, R_> tasksRefreshFailed,
            @Nonnull Function<TasksLoadingFailed, R_> tasksLoadingFailed) {
            return newTaskClicked.apply(this);
        }
    }

    /**
     * NavigateToTaskDetailsRequested
     */
    public static final class NavigateToTaskDetailsRequested extends TasksListEvent {
        private final String taskId;

        NavigateToTaskDetailsRequested(String taskId) {
            this.taskId = checkNotNull(taskId);
        }

        /**
         * taskId
         *
         * @return String
         */
        @Nonnull
        public final String taskId() {
            return taskId;
        }

        @Override
        public boolean equals(Object other) {
            if (other == this) {
                return true;
            }
            if (!(other instanceof NavigateToTaskDetailsRequested)) {
                return false;
            }
            NavigateToTaskDetailsRequested o1 = (NavigateToTaskDetailsRequested) other;
            return o1.taskId.equals(this.taskId);
        }

        @Override
        public int hashCode() {
            int result = 0;
            return result * 31 + taskId.hashCode();
        }

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append("NavigateToTaskDetailsRequested{taskId=").append(taskId);
            return builder.append('}').toString();
        }

        @Override
        public final void match(@Nonnull Consumer<RefreshRequested> refreshRequested,
            @Nonnull Consumer<NewTaskClicked> newTaskClicked,
            @Nonnull Consumer<NavigateToTaskDetailsRequested> navigateToTaskDetailsRequested,
            @Nonnull Consumer<TaskMarkedComplete> taskMarkedComplete,
            @Nonnull Consumer<TaskMarkedActive> taskMarkedActive,
            @Nonnull Consumer<ClearCompletedTasksRequested> clearCompletedTasksRequested,
            @Nonnull Consumer<FilterSelected> filterSelected,
            @Nonnull Consumer<TasksLoaded> tasksLoaded, @Nonnull Consumer<TaskCreated> taskCreated,
            @Nonnull Consumer<TasksRefreshed> tasksRefreshed,
            @Nonnull Consumer<TasksRefreshFailed> tasksRefreshFailed,
            @Nonnull Consumer<TasksLoadingFailed> tasksLoadingFailed) {
            navigateToTaskDetailsRequested.accept(this);
        }

        @Override
        public final <R_> R_ map(@Nonnull Function<RefreshRequested, R_> refreshRequested,
            @Nonnull Function<NewTaskClicked, R_> newTaskClicked,
            @Nonnull Function<NavigateToTaskDetailsRequested, R_> navigateToTaskDetailsRequested,
            @Nonnull Function<TaskMarkedComplete, R_> taskMarkedComplete,
            @Nonnull Function<TaskMarkedActive, R_> taskMarkedActive,
            @Nonnull Function<ClearCompletedTasksRequested, R_> clearCompletedTasksRequested,
            @Nonnull Function<FilterSelected, R_> filterSelected,
            @Nonnull Function<TasksLoaded, R_> tasksLoaded,
            @Nonnull Function<TaskCreated, R_> taskCreated,
            @Nonnull Function<TasksRefreshed, R_> tasksRefreshed,
            @Nonnull Function<TasksRefreshFailed, R_> tasksRefreshFailed,
            @Nonnull Function<TasksLoadingFailed, R_> tasksLoadingFailed) {
            return navigateToTaskDetailsRequested.apply(this);
        }
    }

    /**
     * TaskMarkedComplete
     */
    public static final class TaskMarkedComplete extends TasksListEvent {
        private final String taskId;

        TaskMarkedComplete(String taskId) {
            this.taskId = checkNotNull(taskId);
        }

        /**
         * taskId
         *
         * @return String
         */
        @Nonnull
        public final String taskId() {
            return taskId;
        }

        @Override
        public boolean equals(Object other) {
            if (other == this) {
                return true;
            }
            if (!(other instanceof TaskMarkedComplete)) {
                return false;
            }
            TaskMarkedComplete o1 = (TaskMarkedComplete) other;
            return o1.taskId.equals(this.taskId);
        }

        @Override
        public int hashCode() {
            int result = 0;
            return result * 31 + taskId.hashCode();
        }

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append("TaskMarkedComplete{taskId=").append(taskId);
            return builder.append('}').toString();
        }

        @Override
        public final void match(@Nonnull Consumer<RefreshRequested> refreshRequested,
            @Nonnull Consumer<NewTaskClicked> newTaskClicked,
            @Nonnull Consumer<NavigateToTaskDetailsRequested> navigateToTaskDetailsRequested,
            @Nonnull Consumer<TaskMarkedComplete> taskMarkedComplete,
            @Nonnull Consumer<TaskMarkedActive> taskMarkedActive,
            @Nonnull Consumer<ClearCompletedTasksRequested> clearCompletedTasksRequested,
            @Nonnull Consumer<FilterSelected> filterSelected,
            @Nonnull Consumer<TasksLoaded> tasksLoaded, @Nonnull Consumer<TaskCreated> taskCreated,
            @Nonnull Consumer<TasksRefreshed> tasksRefreshed,
            @Nonnull Consumer<TasksRefreshFailed> tasksRefreshFailed,
            @Nonnull Consumer<TasksLoadingFailed> tasksLoadingFailed) {
            taskMarkedComplete.accept(this);
        }

        @Override
        public final <R_> R_ map(@Nonnull Function<RefreshRequested, R_> refreshRequested,
            @Nonnull Function<NewTaskClicked, R_> newTaskClicked,
            @Nonnull Function<NavigateToTaskDetailsRequested, R_> navigateToTaskDetailsRequested,
            @Nonnull Function<TaskMarkedComplete, R_> taskMarkedComplete,
            @Nonnull Function<TaskMarkedActive, R_> taskMarkedActive,
            @Nonnull Function<ClearCompletedTasksRequested, R_> clearCompletedTasksRequested,
            @Nonnull Function<FilterSelected, R_> filterSelected,
            @Nonnull Function<TasksLoaded, R_> tasksLoaded,
            @Nonnull Function<TaskCreated, R_> taskCreated,
            @Nonnull Function<TasksRefreshed, R_> tasksRefreshed,
            @Nonnull Function<TasksRefreshFailed, R_> tasksRefreshFailed,
            @Nonnull Function<TasksLoadingFailed, R_> tasksLoadingFailed) {
            return taskMarkedComplete.apply(this);
        }
    }

    /**
     * TaskMarkedActive
     */
    public static final class TaskMarkedActive extends TasksListEvent {
        private final String taskId;

        TaskMarkedActive(String taskId) {
            this.taskId = checkNotNull(taskId);
        }

        /**
         * taskId
         *
         * @return String
         */
        @Nonnull
        public final String taskId() {
            return taskId;
        }

        @Override
        public boolean equals(Object other) {
            if (other == this) {
                return true;
            }
            if (!(other instanceof TaskMarkedActive)) {
                return false;
            }
            TaskMarkedActive o1 = (TaskMarkedActive) other;
            return o1.taskId.equals(this.taskId);
        }

        @Override
        public int hashCode() {
            int result = 0;
            return result * 31 + taskId.hashCode();
        }

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append("TaskMarkedActive{taskId=").append(taskId);
            return builder.append('}').toString();
        }

        @Override
        public final void match(@Nonnull Consumer<RefreshRequested> refreshRequested,
            @Nonnull Consumer<NewTaskClicked> newTaskClicked,
            @Nonnull Consumer<NavigateToTaskDetailsRequested> navigateToTaskDetailsRequested,
            @Nonnull Consumer<TaskMarkedComplete> taskMarkedComplete,
            @Nonnull Consumer<TaskMarkedActive> taskMarkedActive,
            @Nonnull Consumer<ClearCompletedTasksRequested> clearCompletedTasksRequested,
            @Nonnull Consumer<FilterSelected> filterSelected,
            @Nonnull Consumer<TasksLoaded> tasksLoaded, @Nonnull Consumer<TaskCreated> taskCreated,
            @Nonnull Consumer<TasksRefreshed> tasksRefreshed,
            @Nonnull Consumer<TasksRefreshFailed> tasksRefreshFailed,
            @Nonnull Consumer<TasksLoadingFailed> tasksLoadingFailed) {
            taskMarkedActive.accept(this);
        }

        @Override
        public final <R_> R_ map(@Nonnull Function<RefreshRequested, R_> refreshRequested,
            @Nonnull Function<NewTaskClicked, R_> newTaskClicked,
            @Nonnull Function<NavigateToTaskDetailsRequested, R_> navigateToTaskDetailsRequested,
            @Nonnull Function<TaskMarkedComplete, R_> taskMarkedComplete,
            @Nonnull Function<TaskMarkedActive, R_> taskMarkedActive,
            @Nonnull Function<ClearCompletedTasksRequested, R_> clearCompletedTasksRequested,
            @Nonnull Function<FilterSelected, R_> filterSelected,
            @Nonnull Function<TasksLoaded, R_> tasksLoaded,
            @Nonnull Function<TaskCreated, R_> taskCreated,
            @Nonnull Function<TasksRefreshed, R_> tasksRefreshed,
            @Nonnull Function<TasksRefreshFailed, R_> tasksRefreshFailed,
            @Nonnull Function<TasksLoadingFailed, R_> tasksLoadingFailed) {
            return taskMarkedActive.apply(this);
        }
    }

    /**
     * ClearCompletedTasksRequested
     */
    public static final class ClearCompletedTasksRequested extends TasksListEvent {
        ClearCompletedTasksRequested() {
        }

        @Override
        public boolean equals(Object other) {
            return other instanceof ClearCompletedTasksRequested;
        }

        @Override
        public int hashCode() {
            return 0;
        }

        @Override
        public String toString() {
            return "ClearCompletedTasksRequested{}";
        }

        @Override
        public final void match(@Nonnull Consumer<RefreshRequested> refreshRequested,
            @Nonnull Consumer<NewTaskClicked> newTaskClicked,
            @Nonnull Consumer<NavigateToTaskDetailsRequested> navigateToTaskDetailsRequested,
            @Nonnull Consumer<TaskMarkedComplete> taskMarkedComplete,
            @Nonnull Consumer<TaskMarkedActive> taskMarkedActive,
            @Nonnull Consumer<ClearCompletedTasksRequested> clearCompletedTasksRequested,
            @Nonnull Consumer<FilterSelected> filterSelected,
            @Nonnull Consumer<TasksLoaded> tasksLoaded, @Nonnull Consumer<TaskCreated> taskCreated,
            @Nonnull Consumer<TasksRefreshed> tasksRefreshed,
            @Nonnull Consumer<TasksRefreshFailed> tasksRefreshFailed,
            @Nonnull Consumer<TasksLoadingFailed> tasksLoadingFailed) {
            clearCompletedTasksRequested.accept(this);
        }

        @Override
        public final <R_> R_ map(@Nonnull Function<RefreshRequested, R_> refreshRequested,
            @Nonnull Function<NewTaskClicked, R_> newTaskClicked,
            @Nonnull Function<NavigateToTaskDetailsRequested, R_> navigateToTaskDetailsRequested,
            @Nonnull Function<TaskMarkedComplete, R_> taskMarkedComplete,
            @Nonnull Function<TaskMarkedActive, R_> taskMarkedActive,
            @Nonnull Function<ClearCompletedTasksRequested, R_> clearCompletedTasksRequested,
            @Nonnull Function<FilterSelected, R_> filterSelected,
            @Nonnull Function<TasksLoaded, R_> tasksLoaded,
            @Nonnull Function<TaskCreated, R_> taskCreated,
            @Nonnull Function<TasksRefreshed, R_> tasksRefreshed,
            @Nonnull Function<TasksRefreshFailed, R_> tasksRefreshFailed,
            @Nonnull Function<TasksLoadingFailed, R_> tasksLoadingFailed) {
            return clearCompletedTasksRequested.apply(this);
        }
    }

    /**
     * FilterSelected
     */
    public static final class FilterSelected extends TasksListEvent {
        private final TasksFilterType filterType;

        FilterSelected(TasksFilterType filterType) {
            this.filterType = checkNotNull(filterType);
        }

        @Nonnull
        public final TasksFilterType filterType() {
            return filterType;
        }

        @Override
        public boolean equals(Object other) {
            if (other == this) {
                return true;
            }
            if (!(other instanceof FilterSelected)) {
                return false;
            }
            FilterSelected o1 = (FilterSelected) other;
            return o1.filterType == filterType;
        }

        @Override
        public int hashCode() {
            int result = 0;
            return result * 31 + filterType.hashCode();
        }

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append("FilterSelected{filterType=").append(filterType);
            return builder.append('}').toString();
        }

        @Override
        public final void match(@Nonnull Consumer<RefreshRequested> refreshRequested,
            @Nonnull Consumer<NewTaskClicked> newTaskClicked,
            @Nonnull Consumer<NavigateToTaskDetailsRequested> navigateToTaskDetailsRequested,
            @Nonnull Consumer<TaskMarkedComplete> taskMarkedComplete,
            @Nonnull Consumer<TaskMarkedActive> taskMarkedActive,
            @Nonnull Consumer<ClearCompletedTasksRequested> clearCompletedTasksRequested,
            @Nonnull Consumer<FilterSelected> filterSelected,
            @Nonnull Consumer<TasksLoaded> tasksLoaded, @Nonnull Consumer<TaskCreated> taskCreated,
            @Nonnull Consumer<TasksRefreshed> tasksRefreshed,
            @Nonnull Consumer<TasksRefreshFailed> tasksRefreshFailed,
            @Nonnull Consumer<TasksLoadingFailed> tasksLoadingFailed) {
            filterSelected.accept(this);
        }

        @Override
        public final <R_> R_ map(@Nonnull Function<RefreshRequested, R_> refreshRequested,
            @Nonnull Function<NewTaskClicked, R_> newTaskClicked,
            @Nonnull Function<NavigateToTaskDetailsRequested, R_> navigateToTaskDetailsRequested,
            @Nonnull Function<TaskMarkedComplete, R_> taskMarkedComplete,
            @Nonnull Function<TaskMarkedActive, R_> taskMarkedActive,
            @Nonnull Function<ClearCompletedTasksRequested, R_> clearCompletedTasksRequested,
            @Nonnull Function<FilterSelected, R_> filterSelected,
            @Nonnull Function<TasksLoaded, R_> tasksLoaded,
            @Nonnull Function<TaskCreated, R_> taskCreated,
            @Nonnull Function<TasksRefreshed, R_> tasksRefreshed,
            @Nonnull Function<TasksRefreshFailed, R_> tasksRefreshFailed,
            @Nonnull Function<TasksLoadingFailed, R_> tasksLoadingFailed) {
            return filterSelected.apply(this);
        }
    }

    /**
     * TasksLoaded
     */
    public static final class TasksLoaded extends TasksListEvent {
        private final ImmutableList<Task> tasks;
        TasksLoaded(ImmutableList<Task> tasks) {
            this.tasks = checkNotNull(tasks);
        }

        /**
         * tasks
         *
         * @return ImmutableList<Task>
         */
        @Nonnull
        public final ImmutableList<Task> tasks() {
            return tasks;
        }

        @Override
        public boolean equals(Object other) {
            if (other == this) {
                return true;
            }
            if (!(other instanceof TasksLoaded)) {
                return false;
            }
            TasksLoaded o1 = (TasksLoaded) other;
            return o1.tasks.equals(this.tasks);
        }

        @Override
        public int hashCode() {
            int result = 0;
            return result * 31 + tasks.hashCode();
        }

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append("TasksLoaded{tasks=").append(tasks);
            return builder.append('}').toString();
        }

        @Override
        public final void match(@Nonnull Consumer<RefreshRequested> refreshRequested,
            @Nonnull Consumer<NewTaskClicked> newTaskClicked,
            @Nonnull Consumer<NavigateToTaskDetailsRequested> navigateToTaskDetailsRequested,
            @Nonnull Consumer<TaskMarkedComplete> taskMarkedComplete,
            @Nonnull Consumer<TaskMarkedActive> taskMarkedActive,
            @Nonnull Consumer<ClearCompletedTasksRequested> clearCompletedTasksRequested,
            @Nonnull Consumer<FilterSelected> filterSelected,
            @Nonnull Consumer<TasksLoaded> tasksLoaded, @Nonnull Consumer<TaskCreated> taskCreated,
            @Nonnull Consumer<TasksRefreshed> tasksRefreshed,
            @Nonnull Consumer<TasksRefreshFailed> tasksRefreshFailed,
            @Nonnull Consumer<TasksLoadingFailed> tasksLoadingFailed) {
            tasksLoaded.accept(this);
        }

        @Override
        public final <R_> R_ map(@Nonnull Function<RefreshRequested, R_> refreshRequested,
            @Nonnull Function<NewTaskClicked, R_> newTaskClicked,
            @Nonnull Function<NavigateToTaskDetailsRequested, R_> navigateToTaskDetailsRequested,
            @Nonnull Function<TaskMarkedComplete, R_> taskMarkedComplete,
            @Nonnull Function<TaskMarkedActive, R_> taskMarkedActive,
            @Nonnull Function<ClearCompletedTasksRequested, R_> clearCompletedTasksRequested,
            @Nonnull Function<FilterSelected, R_> filterSelected,
            @Nonnull Function<TasksLoaded, R_> tasksLoaded,
            @Nonnull Function<TaskCreated, R_> taskCreated,
            @Nonnull Function<TasksRefreshed, R_> tasksRefreshed,
            @Nonnull Function<TasksRefreshFailed, R_> tasksRefreshFailed,
            @Nonnull Function<TasksLoadingFailed, R_> tasksLoadingFailed) {
            return tasksLoaded.apply(this);
        }
    }

    /**
     * TaskCreated
     */
    public static final class TaskCreated extends TasksListEvent {
        TaskCreated() {
        }

        @Override
        public boolean equals(Object other) {
            return other instanceof TaskCreated;
        }

        @Override
        public int hashCode() {
            return 0;
        }

        @Override
        public String toString() {
            return "TaskCreated{}";
        }

        @Override
        public final void match(@Nonnull Consumer<RefreshRequested> refreshRequested,
            @Nonnull Consumer<NewTaskClicked> newTaskClicked,
            @Nonnull Consumer<NavigateToTaskDetailsRequested> navigateToTaskDetailsRequested,
            @Nonnull Consumer<TaskMarkedComplete> taskMarkedComplete,
            @Nonnull Consumer<TaskMarkedActive> taskMarkedActive,
            @Nonnull Consumer<ClearCompletedTasksRequested> clearCompletedTasksRequested,
            @Nonnull Consumer<FilterSelected> filterSelected,
            @Nonnull Consumer<TasksLoaded> tasksLoaded, @Nonnull Consumer<TaskCreated> taskCreated,
            @Nonnull Consumer<TasksRefreshed> tasksRefreshed,
            @Nonnull Consumer<TasksRefreshFailed> tasksRefreshFailed,
            @Nonnull Consumer<TasksLoadingFailed> tasksLoadingFailed) {
            taskCreated.accept(this);
        }

        @Override
        public final <R_> R_ map(@Nonnull Function<RefreshRequested, R_> refreshRequested,
            @Nonnull Function<NewTaskClicked, R_> newTaskClicked,
            @Nonnull Function<NavigateToTaskDetailsRequested, R_> navigateToTaskDetailsRequested,
            @Nonnull Function<TaskMarkedComplete, R_> taskMarkedComplete,
            @Nonnull Function<TaskMarkedActive, R_> taskMarkedActive,
            @Nonnull Function<ClearCompletedTasksRequested, R_> clearCompletedTasksRequested,
            @Nonnull Function<FilterSelected, R_> filterSelected,
            @Nonnull Function<TasksLoaded, R_> tasksLoaded,
            @Nonnull Function<TaskCreated, R_> taskCreated,
            @Nonnull Function<TasksRefreshed, R_> tasksRefreshed,
            @Nonnull Function<TasksRefreshFailed, R_> tasksRefreshFailed,
            @Nonnull Function<TasksLoadingFailed, R_> tasksLoadingFailed) {
            return taskCreated.apply(this);
        }
    }

    /**
     * TasksRefreshed
     */
    public static final class TasksRefreshed extends TasksListEvent {
        TasksRefreshed() {
        }

        @Override
        public boolean equals(Object other) {
            return other instanceof TasksRefreshed;
        }

        @Override
        public int hashCode() {
            return 0;
        }

        @Override
        public String toString() {
            return "TasksRefreshed{}";
        }

        @Override
        public final void match(@Nonnull Consumer<RefreshRequested> refreshRequested,
            @Nonnull Consumer<NewTaskClicked> newTaskClicked,
            @Nonnull Consumer<NavigateToTaskDetailsRequested> navigateToTaskDetailsRequested,
            @Nonnull Consumer<TaskMarkedComplete> taskMarkedComplete,
            @Nonnull Consumer<TaskMarkedActive> taskMarkedActive,
            @Nonnull Consumer<ClearCompletedTasksRequested> clearCompletedTasksRequested,
            @Nonnull Consumer<FilterSelected> filterSelected,
            @Nonnull Consumer<TasksLoaded> tasksLoaded, @Nonnull Consumer<TaskCreated> taskCreated,
            @Nonnull Consumer<TasksRefreshed> tasksRefreshed,
            @Nonnull Consumer<TasksRefreshFailed> tasksRefreshFailed,
            @Nonnull Consumer<TasksLoadingFailed> tasksLoadingFailed) {
            tasksRefreshed.accept(this);
        }

        @Override
        public final <R_> R_ map(@Nonnull Function<RefreshRequested, R_> refreshRequested,
            @Nonnull Function<NewTaskClicked, R_> newTaskClicked,
            @Nonnull Function<NavigateToTaskDetailsRequested, R_> navigateToTaskDetailsRequested,
            @Nonnull Function<TaskMarkedComplete, R_> taskMarkedComplete,
            @Nonnull Function<TaskMarkedActive, R_> taskMarkedActive,
            @Nonnull Function<ClearCompletedTasksRequested, R_> clearCompletedTasksRequested,
            @Nonnull Function<FilterSelected, R_> filterSelected,
            @Nonnull Function<TasksLoaded, R_> tasksLoaded,
            @Nonnull Function<TaskCreated, R_> taskCreated,
            @Nonnull Function<TasksRefreshed, R_> tasksRefreshed,
            @Nonnull Function<TasksRefreshFailed, R_> tasksRefreshFailed,
            @Nonnull Function<TasksLoadingFailed, R_> tasksLoadingFailed) {
            return tasksRefreshed.apply(this);
        }
    }

    /**
     * TasksRefreshFailed
     */
    public static final class TasksRefreshFailed extends TasksListEvent {
        TasksRefreshFailed() {
        }

        @Override
        public boolean equals(Object other) {
            return other instanceof TasksRefreshFailed;
        }

        @Override
        public int hashCode() {
            return 0;
        }

        @Override
        public String toString() {
            return "TasksRefreshFailed{}";
        }

        @Override
        public final void match(@Nonnull Consumer<RefreshRequested> refreshRequested,
            @Nonnull Consumer<NewTaskClicked> newTaskClicked,
            @Nonnull Consumer<NavigateToTaskDetailsRequested> navigateToTaskDetailsRequested,
            @Nonnull Consumer<TaskMarkedComplete> taskMarkedComplete,
            @Nonnull Consumer<TaskMarkedActive> taskMarkedActive,
            @Nonnull Consumer<ClearCompletedTasksRequested> clearCompletedTasksRequested,
            @Nonnull Consumer<FilterSelected> filterSelected,
            @Nonnull Consumer<TasksLoaded> tasksLoaded, @Nonnull Consumer<TaskCreated> taskCreated,
            @Nonnull Consumer<TasksRefreshed> tasksRefreshed,
            @Nonnull Consumer<TasksRefreshFailed> tasksRefreshFailed,
            @Nonnull Consumer<TasksLoadingFailed> tasksLoadingFailed) {
            tasksRefreshFailed.accept(this);
        }

        @Override
        public final <R_> R_ map(@Nonnull Function<RefreshRequested, R_> refreshRequested,
            @Nonnull Function<NewTaskClicked, R_> newTaskClicked,
            @Nonnull Function<NavigateToTaskDetailsRequested, R_> navigateToTaskDetailsRequested,
            @Nonnull Function<TaskMarkedComplete, R_> taskMarkedComplete,
            @Nonnull Function<TaskMarkedActive, R_> taskMarkedActive,
            @Nonnull Function<ClearCompletedTasksRequested, R_> clearCompletedTasksRequested,
            @Nonnull Function<FilterSelected, R_> filterSelected,
            @Nonnull Function<TasksLoaded, R_> tasksLoaded,
            @Nonnull Function<TaskCreated, R_> taskCreated,
            @Nonnull Function<TasksRefreshed, R_> tasksRefreshed,
            @Nonnull Function<TasksRefreshFailed, R_> tasksRefreshFailed,
            @Nonnull Function<TasksLoadingFailed, R_> tasksLoadingFailed) {
            return tasksRefreshFailed.apply(this);
        }
    }

    /**
     * TasksLoadingFailed
     */
    public static final class TasksLoadingFailed extends TasksListEvent {
        TasksLoadingFailed() {
        }

        @Override
        public boolean equals(Object other) {
            return other instanceof TasksLoadingFailed;
        }

        @Override
        public int hashCode() {
            return 0;
        }

        @Override
        public String toString() {
            return "TasksLoadingFailed{}";
        }

        @Override
        public final void match(@Nonnull Consumer<RefreshRequested> refreshRequested,
            @Nonnull Consumer<NewTaskClicked> newTaskClicked,
            @Nonnull Consumer<NavigateToTaskDetailsRequested> navigateToTaskDetailsRequested,
            @Nonnull Consumer<TaskMarkedComplete> taskMarkedComplete,
            @Nonnull Consumer<TaskMarkedActive> taskMarkedActive,
            @Nonnull Consumer<ClearCompletedTasksRequested> clearCompletedTasksRequested,
            @Nonnull Consumer<FilterSelected> filterSelected,
            @Nonnull Consumer<TasksLoaded> tasksLoaded, @Nonnull Consumer<TaskCreated> taskCreated,
            @Nonnull Consumer<TasksRefreshed> tasksRefreshed,
            @Nonnull Consumer<TasksRefreshFailed> tasksRefreshFailed,
            @Nonnull Consumer<TasksLoadingFailed> tasksLoadingFailed) {
            tasksLoadingFailed.accept(this);
        }

        @Override
        public final <R_> R_ map(@Nonnull Function<RefreshRequested, R_> refreshRequested,
            @Nonnull Function<NewTaskClicked, R_> newTaskClicked,
            @Nonnull Function<NavigateToTaskDetailsRequested, R_> navigateToTaskDetailsRequested,
            @Nonnull Function<TaskMarkedComplete, R_> taskMarkedComplete,
            @Nonnull Function<TaskMarkedActive, R_> taskMarkedActive,
            @Nonnull Function<ClearCompletedTasksRequested, R_> clearCompletedTasksRequested,
            @Nonnull Function<FilterSelected, R_> filterSelected,
            @Nonnull Function<TasksLoaded, R_> tasksLoaded,
            @Nonnull Function<TaskCreated, R_> taskCreated,
            @Nonnull Function<TasksRefreshed, R_> tasksRefreshed,
            @Nonnull Function<TasksRefreshFailed, R_> tasksRefreshFailed,
            @Nonnull Function<TasksLoadingFailed, R_> tasksLoadingFailed) {
            return tasksLoadingFailed.apply(this);
        }
    }
}
