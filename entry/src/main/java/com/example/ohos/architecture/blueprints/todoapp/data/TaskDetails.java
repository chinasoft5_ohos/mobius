/*
 * -\-\-
 * --
 * Copyright (c) 2017-2018 Spotify AB
 * --
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * -/-/-
 */

package com.example.ohos.architecture.blueprints.todoapp.data;

import java.io.Serializable;

/**
 * @since 2021-06-08
 */
public class TaskDetails implements Serializable {
    /**
     * TaskDetails DEFAULT
     */
    public static final TaskDetails DEFAULT = TaskDetails.builder().build();
    private final String title;
    private final String description;
    private final boolean completed;

    /**
     * TaskDetails
     *
     * @param title title
     * @param description description
     * @param completed completed
     */
    public TaskDetails(String title, String description, boolean completed) {
        this.title = title;
        this.description = description;
        this.completed = completed;
    }

    /**
     * title
     *
     * @return String
     */
    public String title() {
        return title;
    }

    /**
     * description
     *
     * @return String
     */
    public String description() {
        return description;
    }

    /**
     * completed
     *
     * @return boolean
     */
    public boolean completed() {
        return completed;
    }

    @Override
    public String toString() {
        return "TaskDetails{"
            + "title=" + title + ", "
            + "description=" + description + ", "
            + "completed=" + completed
            + "}";
    }

    /**
     * toBuilder
     *
     * @return Builder
     */
    public TaskDetails.Builder toBuilder() {
        return new Builder(this);
    }

    /**
     * create
     *
     * @param title title
     * @param description description
     * @param completed completed
     * @return TaskDetails
     */
    public static TaskDetails create(String title, String description, boolean completed) {
        return builder().title(title).description(description).completed(completed).build();
    }

    /**
     * create
     *
     * @param title title
     * @param description description
     * @return TaskDetails
     */
    public static TaskDetails create(String title, String description) {
        return builder().title(title).description(description).completed(false).build();
    }

    /**
     * builder
     *
     * @return Builder
     */
    public static Builder builder() {
        return new TaskDetails.Builder().completed(false).title("").description("");
    }

    /**
     * Builder
     */
    public static final class Builder {
        private String title;
        private String description;
        private Boolean completed;

        Builder() {
        }

        private Builder(TaskDetails source) {
            this.title = source.title();
            this.description = source.description();
            this.completed = source.completed();
        }

        /**
         * title
         *
         * @param title title
         * @return TaskDetails.Builder
         */
        public TaskDetails.Builder title(String title) {
            if (title == null) {
                throw new NullPointerException("Null title");
            }
            this.title = title;
            return this;
        }

        /**
         * description
         *
         * @param description description
         * @return TaskDetails.Builder
         */
        public TaskDetails.Builder description(String description) {
            if (description == null) {
                throw new NullPointerException("Null description");
            }
            this.description = description;
            return this;
        }

        /**
         * completed
         *
         * @param completed completed
         * @return TaskDetails.Builder
         */
        public TaskDetails.Builder completed(boolean completed) {
            this.completed = completed;
            return this;
        }

        /**
         * build
         *
         * @return TaskDetails
         */
        public TaskDetails build() {
            String missing = "";
            if (this.title == null) {
                missing += " title";
            }
            if (this.description == null) {
                missing += " description";
            }
            if (this.completed == null) {
                missing += " completed";
            }
            if (!missing.isEmpty()) {
                throw new IllegalStateException("Missing required properties:" + missing);
            }
            return new TaskDetails(
                this.title,
                this.description,
                this.completed);
        }
    }

    /**
     * withCompleted
     *
     * @param completed completed
     * @return TaskDetails
     */
    public TaskDetails withCompleted(boolean completed) {
        return toBuilder().completed(completed).build();
    }

}

