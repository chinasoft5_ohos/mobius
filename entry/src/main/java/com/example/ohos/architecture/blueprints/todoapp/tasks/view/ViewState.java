package com.example.ohos.architecture.blueprints.todoapp.tasks.view;

import com.google.common.collect.ImmutableList;
import com.spotify.dataenum.function.Consumer;
import com.spotify.dataenum.function.Function;

import javax.annotation.Generated;
import javax.annotation.Nonnull;

import static com.spotify.dataenum.DataenumUtils.checkNotNull;

/**
 * ViewState
 */
@Generated("com.spotify.dataenum.processor.DataEnumProcessor")
public abstract class ViewState {
    ViewState() {
    }

    /**
     * awaitingTasks
     *
     * @return ViewState
     */
    public static ViewState awaitingTasks() {
        return new AwaitingTasks();
    }

    /**
     * emptyTasks
     *
     * @param viewData viewData
     * @return ViewState
     */
    public static ViewState emptyTasks(@Nonnull TasksListViewData.EmptyTasksViewData viewData) {
        return new EmptyTasks(viewData);
    }

    /**
     * hasTasks
     *
     * @param taskViewData taskViewData
     * @return ViewState
     */
    public static ViewState hasTasks(@Nonnull ImmutableList<TasksListViewData.TaskViewData> taskViewData) {
        return new HasTasks(taskViewData);
    }

    /**
     * isAwaitingTasks
     *
     * @return boolean
     */
    public final boolean isAwaitingTasks() {
        return (this instanceof AwaitingTasks);
    }

    /**
     * isEmptyTasks
     *
     * @return boolean
     */
    public final boolean isEmptyTasks() {
        return (this instanceof EmptyTasks);
    }

    /**
     * isHasTasks
     *
     * @return boolean
     */
    public final boolean isHasTasks() {
        return (this instanceof HasTasks);
    }

    /**
     * asAwaitingTasks
     *
     * @return AwaitingTasks
     */
    public final AwaitingTasks asAwaitingTasks() {
        return (AwaitingTasks) this;
    }

    /**
     * asEmptyTasks
     *
     * @return EmptyTasks
     */
    public final EmptyTasks asEmptyTasks() {
        return (EmptyTasks) this;
    }

    /**
     * asHasTasks
     *
     * @return HasTasks
     */
    public final HasTasks asHasTasks() {
        return (HasTasks) this;
    }

    /**
     * match
     *
     * @param awaitingTasks awaitingTasks
     * @param emptyTasks emptyTasks
     * @param hasTasks hasTasks
     */
    public abstract void match(@Nonnull Consumer<AwaitingTasks> awaitingTasks,
                               @Nonnull Consumer<EmptyTasks> emptyTasks, @Nonnull Consumer<HasTasks> hasTasks);

    /**
     * map
     *
     * @param awaitingTasks awaitingTasks
     * @param emptyTasks emptyTasks
     * @param hasTasks hasTasks
     * @param <R_> R_
     * @return R_
     */
    public abstract <R_> R_ map(@Nonnull Function<AwaitingTasks, R_> awaitingTasks,
                                @Nonnull Function<EmptyTasks, R_> emptyTasks, @Nonnull Function<HasTasks, R_> hasTasks);

    /**
     * AwaitingTasks
     */
    public static final class AwaitingTasks extends ViewState {
        AwaitingTasks() {
        }

        @Override
        public boolean equals(Object other) {
            return other instanceof ViewState.AwaitingTasks;
        }

        @Override
        public int hashCode() {
            return 0;
        }

        @Override
        public String toString() {
            return "AwaitingTasks{}";
        }

        @Override
        public final void match(@Nonnull Consumer<ViewState.AwaitingTasks> awaitingTasks,
            @Nonnull Consumer<ViewState.EmptyTasks> emptyTasks, @Nonnull Consumer<ViewState.HasTasks> hasTasks) {
            awaitingTasks.accept(this);
        }

        @Override
        public final <R_> R_ map(@Nonnull Function<ViewState.AwaitingTasks, R_> awaitingTasks,
            @Nonnull Function<ViewState.EmptyTasks, R_> emptyTasks, @Nonnull Function<ViewState.HasTasks, R_> hasTasks) {
            return awaitingTasks.apply(this);
        }
    }

    /**
     * EmptyTasks
     */
    public static final class EmptyTasks extends ViewState {
        private final TasksListViewData.EmptyTasksViewData viewData;

        EmptyTasks(TasksListViewData.EmptyTasksViewData viewData) {
            this.viewData = checkNotNull(viewData);
        }

        /**
         * viewData
         *
         * @return TasksListViewData.EmptyTasksViewData
         */
        @Nonnull
        public final TasksListViewData.EmptyTasksViewData viewData() {
            return viewData;
        }

        @Override
        public boolean equals(Object other) {
            if (other == this) {
                return true;
            }
            if (!(other instanceof ViewState.EmptyTasks)) {
                return false;
            }
            ViewState.EmptyTasks o1 = (ViewState.EmptyTasks) other;
            return o1.viewData.equals(this.viewData);
        }

        @Override
        public int hashCode() {
            int result = 0;
            return result * 31 + viewData.hashCode();
        }

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append("EmptyTasks{viewData=").append(viewData);
            return builder.append('}').toString();
        }

        @Override
        public final void match(@Nonnull Consumer<ViewState.AwaitingTasks> awaitingTasks,
                                @Nonnull Consumer<ViewState.EmptyTasks> emptyTasks, @Nonnull Consumer<ViewState.HasTasks> hasTasks) {
            emptyTasks.accept(this);
        }

        @Override
        public final <R_> R_ map(@Nonnull Function<ViewState.AwaitingTasks, R_> awaitingTasks,
            @Nonnull Function<ViewState.EmptyTasks, R_> emptyTasks, @Nonnull Function<ViewState.HasTasks, R_> hasTasks) {
            return emptyTasks.apply(this);
        }
    }

    /**
     * HasTasks
     */
    public static final class HasTasks extends ViewState {
        private final ImmutableList<TasksListViewData.TaskViewData> taskViewData;

        HasTasks(ImmutableList<TasksListViewData.TaskViewData> taskViewData) {
            this.taskViewData = checkNotNull(taskViewData);
        }

        /**
         * taskViewData
         *
         * @return ImmutableList<TasksListViewData.TaskViewData>
         */
        @Nonnull
        public final ImmutableList<TasksListViewData.TaskViewData> taskViewData() {
            return taskViewData;
        }

        @Override
        public boolean equals(Object other) {
            if (other == this) {
                return true;
            }
            if (!(other instanceof ViewState.HasTasks)) {
                return false;
            }
            ViewState.HasTasks o1 = (ViewState.HasTasks) other;
            return o1.taskViewData.equals(this.taskViewData);
        }

        @Override
        public int hashCode() {
            int result = 0;
            return result * 31 + taskViewData.hashCode();
        }

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append("HasTasks{taskViewData=").append(taskViewData);
            return builder.append('}').toString();
        }

        @Override
        public final void match(@Nonnull Consumer<ViewState.AwaitingTasks> awaitingTasks,
            @Nonnull Consumer<ViewState.EmptyTasks> emptyTasks, @Nonnull Consumer<ViewState.HasTasks> hasTasks) {
            hasTasks.accept(this);
        }

        @Override
        public final <R_> R_ map(@Nonnull Function<ViewState.AwaitingTasks, R_> awaitingTasks,
            @Nonnull Function<ViewState.EmptyTasks, R_> emptyTasks, @Nonnull Function<ViewState.HasTasks, R_> hasTasks) {
            return hasTasks.apply(this);
        }
    }
}
